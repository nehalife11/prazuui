using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace PrazuUI.Processor
{
  public static class DBUtility
  {
    public static string GetConnectionString()
    {
      var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
      var Configuration = builder.Build();
      var conn = Configuration["ConnectionStrings:DefaultConnection"];
      return conn;
    }
    public static DataTable GetDataFromDb(string query)
    {
      //var query = "select * from dpact.projectdata;";
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        using (MySqlCommand cmd = new MySqlCommand(query, mysqlConnection))
        {
          cmd.CommandType = CommandType.Text;
          using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
          {
            using (DataTable dt = new DataTable())
            {
              sda.Fill(dt);
              return dt;
            }
          }
        }
      }

    }
    public static DataTable GetDataWithParam(string query, string parameters)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        using (MySqlCommand cmd = new MySqlCommand(query, mysqlConnection))
        {
          cmd.CommandType = CommandType.Text;
          //cmd.Parameters.Add(new MySqlParameter("@city", "Berlin"));
          using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
          {
            using (DataTable dt = new DataTable())
            {
              sda.Fill(dt);
              return dt;
            }
          }
        }
      }
    }

    public static bool insertintoDB(string sqlCommand)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {

        try
        {
          using (MySqlCommand myCmd = new MySqlCommand(sqlCommand, mysqlConnection))
          {
            mysqlConnection.Open();
            myCmd.CommandType = CommandType.Text;
            int result = myCmd.ExecuteNonQuery();
            if (result > 0)
              return true;
            else
              return false;
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }

    public static int retrieveUserId(string sqlCommand)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {

        try
        {
          using (MySqlCommand myCmd = new MySqlCommand(sqlCommand, mysqlConnection))
          {
            mysqlConnection.Open();
            myCmd.CommandType = CommandType.Text;
            var queryResult = myCmd.ExecuteScalar();
            if (queryResult != null)
              return Convert.ToInt32(queryResult);
            else
              return 0;
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }

    public static bool CheckRecord(string sqlCommand)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        try
        {
          using (MySqlCommand myCmd = new MySqlCommand(sqlCommand, mysqlConnection))
          {
            mysqlConnection.Open();
            myCmd.CommandType = CommandType.Text;
            var queryResult = myCmd.ExecuteScalar();
            if (queryResult != null)
              return true;
            else
              return false;
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }


    public static DataTable GetUserDetailsFromDB(Dictionary<string, string> parameters)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {

        try
        {
          using (MySqlCommand cmd = new MySqlCommand("GetUserDetails", mysqlConnection))
          {
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (string key in parameters.Keys)
            {
              cmd.Parameters.Add(key, MySqlDbType.VarChar).Value = parameters[key];
            }
            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
            {
              DataTable dt = new DataTable();
              sda.Fill(dt);
              return dt;
            }
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }

    public static ForgotPassword InsertOTP(ForgotPassword reqObj)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        try
        {
          using (MySqlCommand cmd = new MySqlCommand("GenerateOTP", mysqlConnection))
          {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("OTP", MySqlDbType.VarChar).Value = reqObj.otp;
            cmd.Parameters.Add("email", MySqlDbType.VarChar).Value = reqObj.userEMail;
            mysqlConnection.Open();
            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
            {
              DataTable dt = new DataTable();
              sda.Fill(dt);
              if (dt.Rows.Count > 0)
              {
                reqObj.firstName = Convert.ToString(dt.Rows[0]["UserName"]);
              }
            }
            mysqlConnection.Close();
            return reqObj;
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }

    public static bool ChangePassword(ForgotPassword reqObj)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        try
        {
          using (MySqlCommand cmd = new MySqlCommand("ChangePassword", mysqlConnection))
          {
            var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
            var Configuration = builder.Build();
            var secretKey = Configuration["ConnectionStrings:secretKey"];
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("email", MySqlDbType.VarChar).Value = reqObj.userEMail;
            cmd.Parameters.Add("passwordvalue", MySqlDbType.VarChar).Value = reqObj.passWord;
            cmd.Parameters.Add("secretkey", MySqlDbType.VarChar).Value = secretKey;
            mysqlConnection.Open();
            int result = cmd.ExecuteNonQuery();
            if (result > 0)
              return true;
            else
              return false;
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }

    public static bool VerifyOtp(ForgotPassword reqObj)
    {
      using (MySqlConnection mysqlConnection = new MySqlConnection(GetConnectionString()))
      {
        try
        {
          using (MySqlCommand cmd = new MySqlCommand("VerifyOtp", mysqlConnection))
          {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("email", MySqlDbType.VarChar).Value = reqObj.userEMail;
            cmd.Parameters.Add("otp", MySqlDbType.VarChar).Value = reqObj.otp;
            mysqlConnection.Open();
            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
            {
              DataTable dt = new DataTable();
              sda.Fill(dt);

              mysqlConnection.Close();
              if (dt.Rows.Count > 0)
              {
                return true;
              }
              else
                return false;
            }
          }
        }
        catch (Exception ce)
        {
          throw ce;
        }
        finally
        {
          mysqlConnection.Close();
        }
      }
    }
  }
}
