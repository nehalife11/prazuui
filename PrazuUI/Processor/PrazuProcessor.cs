using Contracts;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PrazuUI.Processor
{
  public static class PrazuProcessor
  {
    public static List<DataTable> GetDataForDropDown()
    {
      List<DataTable> dataTables = new List<DataTable>();
      DataTable dt = new DataTable();
      DataTable data = new DataTable();
      dt = DBUtility.GetDataFromDb(Query.QueryForRoleDropDown);
      data = DBUtility.GetDataFromDb(Query.QueryForProjectDropDown);
      dataTables.Add(dt);
      dataTables.Add(data);
      return dataTables;
    }

    public static bool insertintoDB(string query)
    {
      try
      {
        return DBUtility.insertintoDB(query);
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }

    public static int retrieveUserId()
    {
      return DBUtility.retrieveUserId(Query.RetrieveUserId);
    }

    public static bool insertintoUserMaster(string sqlCommand, Profile profileobj)
    {
      if (!checkUser(profileobj))
      {
        return insertintoDB(sqlCommand);
      }
      return false;
    }

    public static bool checkUser(Profile profileobj)
    {
      string checkQuery = Query.CheckUserNameinDB + "'" + profileobj.UserMap.UserName + "'";
      return DBUtility.CheckRecord(checkQuery);
    }

    public static Profile GetUserDetails(Profile userObj)
    {
      return MapDataTabletoProfile(DBUtility.GetUserDetailsFromDB(createParameterObject(userObj)));
    }

    public static Profile MapDataTabletoProfile(DataTable dt)
    {
      if (dt.Rows.Count > 0)
      {
        Profile profileObj = new Profile();
        profileObj.ProfileId = Convert.ToInt32(dt.Rows[0]["ProfileId"]);
        profileObj.RoleMap.RoleID = Convert.ToInt32(dt.Rows[0]["RoleId"]);
        profileObj.RoleMap.RoleName = Convert.ToString(dt.Rows[0]["RoleName"]);
        profileObj.ProjectMap.ProjectId = Convert.ToInt32(dt.Rows[0]["ProjectId"]);
        profileObj.ProjectMap.ProjectName = Convert.ToString(dt.Rows[0]["ProjectName"]);
        profileObj.ProjectMap.ProjectType = Convert.ToString(dt.Rows[0]["ProjectType"]);
        profileObj.ProjectMap.ProjectSPOC = Convert.ToString(dt.Rows[0]["ProjectSPOC"]);
        profileObj.UserMap.UserEmail = Convert.ToString(dt.Rows[0]["useremail"]);
        profileObj.UserMap.UserName = Convert.ToString(dt.Rows[0]["username"]);
        profileObj.AssetMap = dt.AsEnumerable().Select(row =>
                                new Asset
                                {
                                  AssetId = row.Field<Int32>("AssetId"),
                                  AssetName = row.Field<string>("AssetName")
                                }).ToList();

        return profileObj;
      }
      else
      {
        return null;
      }
    }

    public static Dictionary<string, string> createParameterObject(Profile userObj)
    {
      var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
      var Configuration = builder.Build();
      var secretKey = Configuration["ConnectionStrings:secretKey"];
      Dictionary<string, string> parameters = new Dictionary<string, string>();
      parameters.Add("usernam", userObj.UserMap.UserName);
      parameters.Add("pass", userObj.UserMap.UserPassword);
      parameters.Add("secretKey", secretKey);
      return parameters;
    }

    public static bool generateOTP(ForgotPassword reqObj)
    {
      try
      {
        reqObj.otp = CreateRandomPassword(10);
        DBUtility.InsertOTP(reqObj);
        if (!string.IsNullOrEmpty(reqObj.firstName))
        {
          MailUtility.SendEmail(reqObj);
          return true;
        }
        return false;
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }

    private static string CreateRandomPassword(int passwordLength)
    {
      string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
      char[] chars = new char[passwordLength];
      Random rd = new Random();

      for (int i = 0; i < passwordLength; i++)
      {
        chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
      }

      return new string(chars);
    }

    public static bool changePassword(ForgotPassword reqObj)
    {
      return DBUtility.ChangePassword(reqObj);
    }

    public static bool verifyOtp(ForgotPassword reqObj)
    {
      return DBUtility.VerifyOtp(reqObj);
    }
  }
}
