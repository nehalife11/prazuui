using Contracts;
using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrazuUI.Processor
{
  public static class MailUtility
  {

    public static void SendEmail(ForgotPassword reqObj)
    {
      Application app = new Application();
      MailItem mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
      mailItem.Subject = "One-time password for Prazu";
      mailItem.To = "pamody@deloitte.com"; // reqObj.userEMail;
      mailItem.HTMLBody = "Hi " + reqObj.firstName + ", <br> This is your one-time password - " + reqObj.otp + "<br> Thanks & Regards,<br> Prazu Team";

      mailItem.Importance = OlImportance.olImportanceNormal;
      mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
      mailItem.Send();
    }
  }
}
