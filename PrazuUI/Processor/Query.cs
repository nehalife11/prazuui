using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrazuUI.Processor
{
  public static class Query
  {
    public static string QueryForRoleDropDown = @"Select RoleId as val,RoleDesc as data from RoleMaster";
    public static string QueryForProjectDropDown = @"Select ProjectId as val,ProjectName as data from ProjectMaster";
    public static string QueryToInsertUser = @"INSERT INTO USERMASTER(USERNAME,USERPASSWORD,USEREMAIL) VALUES";
    public static string QueryToInsertProfile = @"INSERT INTO USERPROFILE(USERID,ROLEID,PROJECTID) VALUES";
    public static string RetrieveUserId = @"Select UserId from UserMaster Order By UserId desc Limit 1";
    public static string CheckUserNameinDB = @"Select UserEmail from UserMaster WHERE UserEmail = ";
    public static string CheckLogIn = "SELECT UserId FROM usermaster where UserName = '@userName' AND UserPassword = AES_ENCRYPT('@password', '@secretKey')";
    public static string InsertOTP = "UPDATE USERMASTER SET InviteCode = '@OTP' WHERE UserEmail = '@UserEmail'";
  }
}
