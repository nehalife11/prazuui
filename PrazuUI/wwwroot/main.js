(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./test-form/test-form.component */ "./src/app/test-form/test-form.component.ts");
/* harmony import */ var _test_results_test_results_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./test-results/test-results.component */ "./src/app/test-results/test-results.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
    },
    {
        path: 'register',
        component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"]
    },
    {
        path: 'form',
        component: _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_4__["TestFormComponent"]
    },
    {
        path: 'results',
        component: _test_results_test_results_component__WEBPACK_IMPORTED_MODULE_5__["TestResultsComponent"]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n\r\n<div class=\"container-fluid\">\r\n    <app-navbar></app-navbar>\r\n    <app-global-spinner [hidden]=\"hideSpinner\"></app-global-spinner>\r\n    <div class=\"main-content\">\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n    <app-footer></app-footer>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid {\n  background: -webkit-linear-gradient(left, #003A7E, #0086C6);\n  font-family: 'Montserrat', sans-serif; }\n\n.main-content {\n  padding: 80px 0;\n  min-height: 85vh; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _spinner_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./spinner-service.service */ "./src/app/spinner-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(spinnerService) {
        this.spinnerService = spinnerService;
        this.hideSpinner = true;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.spinnerService.change.subscribe(function (showSpinner) {
            _this.hideSpinner = !showSpinner;
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_spinner_service_service__WEBPACK_IMPORTED_MODULE_1__["SpinnerServiceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./test-form/test-form.component */ "./src/app/test-form/test-form.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _global_spinner_global_spinner_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./global-spinner/global-spinner.component */ "./src/app/global-spinner/global-spinner.component.ts");
/* harmony import */ var _test_results_test_results_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./test-results/test-results.component */ "./src/app/test-results/test-results.component.ts");
/* harmony import */ var ngx_tooltip__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-tooltip */ "./node_modules/ngx-tooltip/index.js");
/* harmony import */ var ngx_tooltip__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ngx_tooltip__WEBPACK_IMPORTED_MODULE_13__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"],
                _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_8__["TestFormComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _global_spinner_global_spinner_component__WEBPACK_IMPORTED_MODULE_11__["GlobalSpinnerComponent"],
                _test_results_test_results_component__WEBPACK_IMPORTED_MODULE_12__["TestResultsComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ngx_tooltip__WEBPACK_IMPORTED_MODULE_13__["TooltipModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12 app-footer text-center\">\r\n    <div class=\"row\">\r\n        Copyright © 2018 Prazu.\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-footer {\n  background: #00357A;\n  height: 70px;\n  color: #ffffff;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  font-size: 1.2em;\n  font-weight: 500;\n  letter-spacing: 1px; }\n"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/global-spinner/global-spinner.component.html":
/*!**************************************************************!*\
  !*** ./src/app/global-spinner/global-spinner.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"global-spinner text-center\">\r\n    <img src=\"../../assets/icons/preloader.gif\" alt=\"Spinner Loader\">\r\n</div>"

/***/ }),

/***/ "./src/app/global-spinner/global-spinner.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/global-spinner/global-spinner.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".global-spinner {\n  position: fixed;\n  display: block;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: #3a38388c;\n  z-index: 100; }\n  .global-spinner img {\n    position: relative;\n    top: 30%; }\n"

/***/ }),

/***/ "./src/app/global-spinner/global-spinner.component.ts":
/*!************************************************************!*\
  !*** ./src/app/global-spinner/global-spinner.component.ts ***!
  \************************************************************/
/*! exports provided: GlobalSpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalSpinnerComponent", function() { return GlobalSpinnerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GlobalSpinnerComponent = /** @class */ (function () {
    function GlobalSpinnerComponent() {
    }
    GlobalSpinnerComponent.prototype.ngOnInit = function () {
    };
    GlobalSpinnerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-global-spinner',
            template: __webpack_require__(/*! ./global-spinner.component.html */ "./src/app/global-spinner/global-spinner.component.html"),
            styles: [__webpack_require__(/*! ./global-spinner.component.scss */ "./src/app/global-spinner/global-spinner.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GlobalSpinnerComponent);
    return GlobalSpinnerComponent;
}());



/***/ }),

/***/ "./src/app/login-service.service.ts":
/*!******************************************!*\
  !*** ./src/app/login-service.service.ts ***!
  \******************************************/
/*! exports provided: LoginServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginServiceService", function() { return LoginServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./spinner-service.service */ "./src/app/spinner-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginServiceService = /** @class */ (function () {
    function LoginServiceService(http, router, spinnerService) {
        this.http = http;
        this.router = router;
        this.spinnerService = spinnerService;
    }
    LoginServiceService.prototype.fireRequest = function (requestObject) {
        var _this = this;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'text/json' })
        };
        return this.http.post('http://localhost:8082/Automation/services/automation', requestObject, httpOptions).subscribe(function (result) {
            console.log("Result is:", result);
            _this.spinnerService.displaySpinner(false);
            _this.router.navigateByUrl('/form');
        }, function (error) { console.log('There was an error: '); _this.spinnerService.displaySpinner(false); });
    };
    LoginServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerServiceService"]])
    ], LoginServiceService);
    return LoginServiceService;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xs-offset-1 col-xs-10 col-sm-8 col-sm-offset-2 col-lg-4 col-lg-offset-4 form-signin\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12 form-signin-icon text-center\">\r\n                <img src=\"../../assets/icons/user.svg\" alt=\"Kiwi standing on oval\">\r\n            </div>\r\n            <div class=\"col-sm-offset-1 col-sm-10 form-signin-inputs\">\r\n                <p class=\"title text-center\">Sign In</p>\r\n\r\n                <input type=\"text\" placeholder=\"User Name\" [(ngModel)]=\"userName\">\r\n\r\n                <input type=\"password\" placeholder=\"Password\" [(ngModel)]=\"password\">\r\n\r\n                <button class=\"btn btn-login\" (click)=\"handleLogin()\">Login</button>\r\n            </div>\r\n            <div class=\"col-sm-12 form-signin-register\">\r\n                <p>Don't have an account ? <span><a routerLink=\"/register\" href=\"#\">REGISTER HERE</a></span></p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-signin {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  padding-bottom: 30px; }\n  .form-signin .form-signin-icon {\n    position: relative;\n    top: -40px; }\n  .form-signin .form-signin-icon img {\n      border: 1px solid #ffffff;\n      border-radius: 40px; }\n  .form-signin .form-signin-inputs {\n    margin-top: -25px; }\n  .form-signin .form-signin-inputs input {\n      width: 100%;\n      margin-top: 20px;\n      padding: 12px;\n      border-radius: 8px;\n      border: 1px solid #ffffff; }\n  .form-signin .form-signin-inputs .title {\n      font-size: 2.2em;\n      color: #ffffff;\n      font-weight: 500; }\n  .form-signin .form-signin-inputs .btn-login {\n      width: 100%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-signin .form-signin-register {\n    text-align: center;\n    font-size: 1em;\n    color: #ffffff;\n    font-weight: 600; }\n  .form-signin .form-signin-register a {\n      color: #AACB2F;\n      font-weight: 700; }\n  .form-signin .tool-tip {\n    width: 25px;\n    height: 25px;\n    background: url(\"data:image/svg+xml,%3Csvg xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 width%3D%2224%22 height%3D%2224%22 viewBox%3D%220 0 24 24%22%3E%3Cpath fill%3D%22%23ffffff%22 d%3D%22M11 15h2v2h-2v-2zm0-8h2v6h-2V7zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z%22%2F%3E%3C%2Fsvg%3E\"); }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login-service.service */ "./src/app/login-service.service.ts");
/* harmony import */ var _spinner_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../spinner-service.service */ "./src/app/spinner-service.service.ts");
/* harmony import */ var _update_navbar_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../update-navbar.service */ "./src/app/update-navbar.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(data, spinnerService, router, navBarService) {
        this.data = data;
        this.spinnerService = spinnerService;
        this.router = router;
        this.navBarService = navBarService;
        this.userName = '';
        this.password = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.navBarService.updateNavBar(true);
    };
    LoginComponent.prototype.handleLogin = function () {
        var reqObj = {
            'username': this.userName,
            'password': this.password
        };
        this.spinnerService.displaySpinner(true);
        //this.data.fireRequest(reqObj);
        this.mockLoginValidation();
    };
    LoginComponent.prototype.mockLoginValidation = function () {
        if (this.userName === 'user' && this.password === 'password') {
            this.router.navigateByUrl('/form');
            this.spinnerService.displaySpinner(false);
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_login_service_service__WEBPACK_IMPORTED_MODULE_1__["LoginServiceService"], _spinner_service_service__WEBPACK_IMPORTED_MODULE_2__["SpinnerServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _update_navbar_service__WEBPACK_IMPORTED_MODULE_3__["UpdateNavbarService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-sm-12 app-header\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-2 title\">\r\n                <a routerLink=\"\"> PRAZU</a>\r\n            </div>\r\n            <div *ngIf=\"isRoot\" class=\"col-sm-8 pull-right nav-items\">\r\n                <a routerLink=\"\" [class.activated]=\"currentUrl == '/'\">Login</a>\r\n                <a routerLink=\"register\" [class.activated]=\"currentUrl == '/register'\">Register</a>\r\n                <!-- <a routerLink=\"form\" [class.activated]=\"currentUrl == '/form'\">Test Form</a> -->\r\n            </div>\r\n            <div *ngIf=\"!isRoot\" class=\"col-sm-8 pull-right nav-items\">\r\n                <span>Welcome User</span>\r\n                <a routerLink=\"\" class=\"btn\" [class.activated]=\"currentUrl == '/'\">LogOut</a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/navbar/navbar.component.scss":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-header {\n  background: #00357A;\n  height: 70px;\n  color: #ffffff;\n  padding-top: 15px; }\n  .app-header .title {\n    margin-left: 40px; }\n  .app-header .title a {\n      font-size: 2em;\n      letter-spacing: 16px;\n      font-weight: 700;\n      color: #AACB2F;\n      text-decoration: none; }\n  .app-header .nav-items {\n    font-size: 1.2em;\n    text-align: right;\n    padding-top: 10px; }\n  .app-header .nav-items a {\n      color: #ffffff;\n      margin: 0px 40px;\n      font-weight: 500;\n      letter-spacing: 2px;\n      cursor: pointer; }\n  .app-header .nav-items a.btn {\n      background: #ffffff;\n      color: #000000;\n      margin-top: -10px; }\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _update_navbar_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../update-navbar.service */ "./src/app/update-navbar.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(navBarService) {
        this.navBarService = navBarService;
        this.isRoot = true;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.navBarService.updateNav.subscribe(function (isRoot) {
            _this.isRoot = isRoot;
        });
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_update_navbar_service__WEBPACK_IMPORTED_MODULE_1__["UpdateNavbarService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/register-service.service.ts":
/*!*********************************************!*\
  !*** ./src/app/register-service.service.ts ***!
  \*********************************************/
/*! exports provided: RegisterServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterServiceService", function() { return RegisterServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./spinner-service.service */ "./src/app/spinner-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterServiceService = /** @class */ (function () {
    function RegisterServiceService(http, router, spinnerService) {
        this.http = http;
        this.router = router;
        this.spinnerService = spinnerService;
    }
    RegisterServiceService.prototype.fireRequest = function (requestObject) {
        var _this = this;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'multipart/form-data' })
        };
        return this.http.post('http://localhost:8082/Automation/services/automation', requestObject, httpOptions).subscribe(function (result) {
            console.log("Result is:", result);
            _this.spinnerService.displaySpinner(false);
            _this.router.navigateByUrl('/');
        }, function (error) { console.log('There was an error: '); _this.spinnerService.displaySpinner(false); });
    };
    RegisterServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerServiceService"]])
    ], RegisterServiceService);
    return RegisterServiceService;
}());



/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xs-offset-1 col-xs-10 col-sm-8 col-sm-offset-2 col-lg-4 col-lg-offset-4 form-register\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12 form-register-icon text-center\">\r\n                <img src=\"../../assets/icons/register.svg\" alt=\"Kiwi standing on oval\">\r\n            </div>\r\n            <div class=\"col-sm-offset-1 col-sm-10 form-register-inputs\">\r\n                <p class=\"title text-center\">Register</p>\r\n\r\n                <input type=\"text\" placeholder=\"First Name\" [(ngModel)]=\"firstName\">\r\n\r\n                <input type=\"text\" placeholder=\"Last Name\" [(ngModel)]=\"lastName\">\r\n\r\n                <input type=\"email\" placeholder=\"Email\" [(ngModel)]=\"email\">\r\n\r\n                <input type=\"password\" placeholder=\"Password\" [(ngModel)]=\"password\">\r\n\r\n                <input type=\"password\" placeholder=\"Confirm Password\" [(ngModel)]=\"confirmPassword\">\r\n\r\n                <button class=\"btn btn-login\" (click)=\"handleRegister()\">Register</button>\r\n            </div>\r\n            <div class=\"col-sm-12 form-register-login\">\r\n                <p>Already have an account ? <span><a routerLink=\"/\" href=\"#\">SIGN IN</a></span></p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/register/register.component.scss":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-register {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  padding-bottom: 30px; }\n  .form-register .form-register-icon {\n    position: relative;\n    top: -40px; }\n  .form-register .form-register-icon img {\n      border: 1px solid #ffffff;\n      border-radius: 40px; }\n  .form-register .form-register-inputs {\n    margin-top: -25px; }\n  .form-register .form-register-inputs input {\n      width: 100%;\n      margin-top: 20px;\n      padding: 12px;\n      border-radius: 8px;\n      border: 1px solid #ffffff; }\n  .form-register .form-register-inputs .title {\n      font-size: 2.2em;\n      color: #ffffff;\n      font-weight: 500; }\n  .form-register .form-register-inputs .btn-login {\n      width: 100%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-register .form-register-login {\n    text-align: center;\n    font-size: 1em;\n    color: #ffffff;\n    font-weight: 600; }\n  .form-register .form-register-login a {\n      color: #AACB2F;\n      font-weight: 700; }\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _register_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../register-service.service */ "./src/app/register-service.service.ts");
/* harmony import */ var _update_navbar_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../update-navbar.service */ "./src/app/update-navbar.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(data, router, navBarService) {
        this.data = data;
        this.router = router;
        this.navBarService = navBarService;
        this.firstName = '';
        this.lastName = '';
        this.email = '';
        this.password = '';
        this.confirmPassword = '';
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.navBarService.updateNavBar(true);
    };
    RegisterComponent.prototype.handleRegister = function () {
        var reqObj = {
            'fname': this.firstName,
            'lname': this.lastName,
            'email': this.email,
            'password': this.confirmPassword
        };
        //this.data.fireRequest(reqObj);
        if (this.password === this.confirmPassword) {
            this.router.navigateByUrl('/form');
        }
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [_register_service_service__WEBPACK_IMPORTED_MODULE_1__["RegisterServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _update_navbar_service__WEBPACK_IMPORTED_MODULE_2__["UpdateNavbarService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/run-tests.service.ts":
/*!**************************************!*\
  !*** ./src/app/run-tests.service.ts ***!
  \**************************************/
/*! exports provided: RunTestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunTestsService", function() { return RunTestsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./spinner-service.service */ "./src/app/spinner-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RunTestsService = /** @class */ (function () {
    function RunTestsService(http, router, spinnerService) {
        this.http = http;
        this.router = router;
        this.spinnerService = spinnerService;
    }
    RunTestsService.prototype.postDetails = function (requestObject) {
        var _this = this;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
        return this.http.post('http://localhost:40506/api/values', requestObject, httpOptions).subscribe(function (result) {
            console.log("Result is:", result);
            _this.spinnerService.displaySpinner(false);
            _this.router.navigateByUrl('/results');
        }, function (error) { console.log('There was an error: '); _this.spinnerService.displaySpinner(false); });
    };
    RunTestsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _spinner_service_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerServiceService"]])
    ], RunTestsService);
    return RunTestsService;
}());



/***/ }),

/***/ "./src/app/spinner-service.service.ts":
/*!********************************************!*\
  !*** ./src/app/spinner-service.service.ts ***!
  \********************************************/
/*! exports provided: SpinnerServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpinnerServiceService", function() { return SpinnerServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SpinnerServiceService = /** @class */ (function () {
    function SpinnerServiceService() {
        this.isShow = false;
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SpinnerServiceService.prototype.displaySpinner = function (toShow) {
        this.isShow = toShow;
        this.change.emit(this.isShow);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], SpinnerServiceService.prototype, "change", void 0);
    SpinnerServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], SpinnerServiceService);
    return SpinnerServiceService;
}());



/***/ }),

/***/ "./src/app/test-form/test-form.component.html":
/*!****************************************************!*\
  !*** ./src/app/test-form/test-form.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 form-test\">\r\n        <p class=\"title text-center\">USER DASHBOARD</p>\r\n        <div class=\"row\">\r\n            <div class=\"col-xs-12 col-sm-12 form-test-dependancies text-center\">\r\n                <div class=\"row\">\r\n                    <p class=\"dependence-text\">You should download all the Selenium dependencies to your local machine. Please click below to proceed further.</p>\r\n                    <a href=\"../../assets/downloadables/SeleniumDependencies.zip\" (click)=\"onDownload()\" target=\"_self\" class=\"btn btn-danger\">Download Dependencies</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-test-container\" id=\"formContainer\">\r\n                <div class=\"col-xs-12 col-sm-12 form-test-fields\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-12 col-lg-6\">\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"projName\">Project Name</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                            </div>\r\n                            <div class=\"input col-xs-12 col-sm-6\">\r\n                                <input id=\"projName\" [(ngModel)]=\"project\">\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"browserName\">Browser</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                            </div>\r\n                            <div class=\"input col-xs-12 col-sm-6\">\r\n                                <select id=\"browserName\" [(ngModel)]=\"browser\">\r\n                                  <option value=\"chrome\">Chrome</option>\r\n                                  <option value=\"saab\">Safari</option>\r\n                                  <option value=\"mercedes\">IE</option>\r\n                                  <option value=\"audi\">Firefox</option>\r\n                                </select>\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"snapshot\">Snapshot</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                            </div>\r\n                            <div class=\"input col-xs-12 col-sm-6\">\r\n                              <select id=\"snapshot\" [(ngModel)]=\"snapshot\">\r\n                                <option value=\"browser\">Browser</option>\r\n                              </select>\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"hostName\">Host Name</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                            </div>\r\n                            <div class=\"input col-xs-12 col-sm-6\">\r\n                                <input id=\"hostName\" [(ngModel)]=\"host\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-sm-12 col-lg-6\">\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"release\">Release Name</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                              </div>\r\n                              <div class=\"input col-xs-12 col-sm-6\">\r\n                                <input id=\"release\" [(ngModel)]=\"release\">\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"url\">URL</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                              </div>\r\n                              <div class=\"input col-xs-12 col-sm-6\">\r\n                                <input id=\"url\" [(ngModel)]=\"url\">\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"snapshotType\">Snapshot Type</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                              </div>\r\n                              <div class=\"input col-xs-12 col-sm-6\">\r\n                                <select id=\"snapshotType\" [(ngModel)]=\"snapshotType\">\r\n                              <option value=\"Yes\">Yes</option>\r\n                              <option value=\"No\">No</option>\r\n                            </select>\r\n                            </div>\r\n                            <div class=\"label col-xs-12 col-sm-6\">\r\n                                <label for=\"localMachine\">Local Machine</label>\r\n                                <p class=\"tool-tip\" tooltipPlacement=\"top\" tooltip=\"Hello fact!\"></p>\r\n                              </div>\r\n                              <div class=\"input col-xs-12 col-sm-6\">\r\n                                <select for=\"localMachine\" [(ngModel)]=\"localMachine\">\r\n                          <option value=\"browser\">Local Machine</option>\r\n                          </select>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xs-offset-0 col-xs-12 col-sm-offset-2 col-sm-8 form-test-uploads\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-xs-12 col-sm-12 upload-option\">\r\n                            <div class=\"col-xs-12 col-sm-6 header col-xs-push-0 col-sm-push-2\">Upload Execution Manager</div>\r\n                            <div class=\"col-xs-12 col-sm-5 action\">\r\n                                <input id=\"execMgr\" (change)=\"onFileUpload($event)\" type=\"file\" class=\"btn\" [(ngModel)]=\"execMgr\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-xs-12 col-sm-12 terms text-center\">\r\n                            <input type=\"checkbox\" name=\"tc\" [(ngModel)]=\"isSfdcProj\" required #tc=\"ngModel\">\r\n                            <span>Is this a SFDC/Salesforce project ?</span>\r\n                        </div>\r\n                        <div class=\"col-xs-12 col-sm-12 upload-option\" [hidden]=\"!isSfdcProj\">\r\n                            <div class=\"col-xs-12 col-sm-6 header col-xs-push-0 col-sm-push-2\">Upload Mapping</div>\r\n                            <div class=\"col-xs-12 col-sm-5 action\">\r\n                                <input id=\"mapFile\" (change)=\"onFileUpload($event)\" type=\"file\" class=\"btn\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-xs-offset-0 col-xs-12 col-sm-offset-4 col-sm-4 text-center\">\r\n                            <button (click)=\"onProceed()\" [disabled]=\"!showForm\">Proceed</button>\r\n                            <p class=\"error-msg\" [hidden]=\"!showFormError\">Please fill in all details.</p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-xs-offset-0 col-xs-12 col-sm-offset-2 col-sm-8 form-final-uploads\" [hidden]=\"!showTestUpload\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-xs-12 col-sm-12 upload-option\">\r\n                        <div class=\"col-xs-12 col-sm-6 header col-xs-push-0 col-sm-push-2\">Upload Test Creation Files</div>\r\n                        <div class=\"col-xs-12 col-sm-5 action\">\r\n                            <input id=\"testCreation\" (change)=\"onFileUpload($event)\" type=\"file\" class=\"btn\" multiple>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-xs-12 col-sm-12 form-test-results text-center\" [hidden]=\"!showLastStep\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-xs-12 col-sm-12 terms-conditions\">\r\n                        <input type=\"checkbox\" name=\"tc\" [(ngModel)]=\"isTCAccepted\" required #tc=\"ngModel\">\r\n                        <span>I ensure that I downloaded all dependant files and Selenium stand alone server is up and running in my local machine before proceeding.</span>\r\n                    </div>\r\n                    <div class=\"col-xs-offset-0 col-xs-12 col-sm-offset-4 col-sm-4\">\r\n                        <button (click)=\"onSubmit()\" [disabled]=\"!isTCAccepted\">Save & Continue</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/test-form/test-form.component.scss":
/*!****************************************************!*\
  !*** ./src/app/test-form/test-form.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-test {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  padding-bottom: 30px; }\n  .form-test .error-msg {\n    font-size: 1em;\n    font-weight: 500;\n    color: red;\n    margin-top: -10px; }\n  .form-test .title {\n    font-size: 2em;\n    font-weight: 700;\n    letter-spacing: 2px;\n    color: #ffffff;\n    margin-top: 10px; }\n  .form-test .label {\n    color: #ffffff;\n    text-align: right;\n    font-size: 1.2em; }\n  .form-test .label label {\n      font-weight: 500; }\n  .form-test .label .tool-tip {\n      display: inline-block;\n      top: 17px;\n      left: 5px;\n      position: relative; }\n  .form-test input,\n  .form-test select {\n    width: 100%;\n    margin-top: 20px;\n    border-radius: 8px;\n    border: 2px solid #AACB2F;\n    height: 35px;\n    background: transparent;\n    padding: 0 10px;\n    color: #ffffff; }\n  .form-test .form-test-dependancies {\n    margin: 20px 0;\n    text-align: center;\n    color: #ffffff; }\n  .form-test .form-test-dependancies a {\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #ffa51f;\n      border-radius: 0;\n      padding: 15px;\n      letter-spacing: 2px;\n      margin-top: 20px;\n      border-color: transparent; }\n  .form-test .form-test-uploads,\n  .form-test .form-final-uploads {\n    margin-top: 40px;\n    color: #ffffff;\n    font-weight: 500;\n    font-size: 1.2em; }\n  .form-test .form-test-uploads .upload-option,\n    .form-test .form-final-uploads .upload-option {\n      margin-top: 15px; }\n  .form-test .form-test-uploads input,\n    .form-test .form-final-uploads input {\n      margin-top: 0;\n      width: 100%;\n      padding: 5px; }\n  .form-test .form-test-uploads .terms,\n    .form-test .form-final-uploads .terms {\n      color: #ffffff;\n      margin-top: 15px; }\n  .form-test .form-test-uploads .terms input[type=checkbox],\n      .form-test .form-final-uploads .terms input[type=checkbox] {\n        width: 15px;\n        height: 20px;\n        position: relative;\n        top: 5px;\n        right: 5px; }\n  .form-test .form-test-uploads button,\n    .form-test .form-final-uploads button {\n      width: 80%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-test .form-test-uploads button:disabled,\n    .form-test .form-final-uploads button:disabled {\n      background: #cccccc; }\n  .form-test .form-test-results {\n    margin-top: 40px; }\n  .form-test .form-test-results .terms-conditions {\n      color: #ffffff; }\n  .form-test .form-test-results .terms-conditions input[type=checkbox] {\n        width: 15px;\n        height: 20px;\n        position: relative;\n        top: 5px;\n        right: 5px; }\n  .form-test .form-test-results button {\n      width: 80%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-test .form-test-results button:disabled {\n      background: #cccccc; }\n  .form-test .form-final-uploads {\n    margin-top: 0; }\n  .form-test .tool-tip {\n    width: 25px;\n    height: 25px;\n    background: url(\"data:image/svg+xml,%3Csvg xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 width%3D%2224%22 height%3D%2224%22 viewBox%3D%220 0 24 24%22%3E%3Cpath fill%3D%22%23ffffff%22 d%3D%22M11 15h2v2h-2v-2zm0-8h2v6h-2V7zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z%22%2F%3E%3C%2Fsvg%3E\"); }\n  @media (max-width: 767px) {\n    .form-test .label {\n      text-align: left; }\n    .form-test .terms {\n      margin-top: 15px; }\n      .form-test .terms input[type=checkbox] {\n        width: 15px;\n        height: 20px;\n        position: relative;\n        top: 5px;\n        right: 5px; } }\n"

/***/ }),

/***/ "./src/app/test-form/test-form.component.ts":
/*!**************************************************!*\
  !*** ./src/app/test-form/test-form.component.ts ***!
  \**************************************************/
/*! exports provided: TestFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestFormComponent", function() { return TestFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _run_tests_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../run-tests.service */ "./src/app/run-tests.service.ts");
/* harmony import */ var _update_navbar_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../update-navbar.service */ "./src/app/update-navbar.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TestFormComponent = /** @class */ (function () {
    function TestFormComponent(data, navBarService) {
        this.data = data;
        this.navBarService = navBarService;
        this.project = '';
        this.browser = '';
        this.snapshot = '';
        this.host = '192.168.163.225';
        this.release = '';
        this.url = '';
        this.snapshotType = '';
        this.localMachine = '';
        this.reqObj = {};
        this.isTCAccepted = false;
        this.isSfdcProj = false;
        this.importedFileName = '';
        this.showTestUpload = false;
        this.showForm = false;
        this.isUploadDone = false;
        this.showLastStep = false;
        this.showFormError = false;
    }
    TestFormComponent.prototype.ngOnInit = function () {
        this.navBarService.updateNavBar(false);
    };
    TestFormComponent.prototype.onFileUpload = function (event) {
        var files = event.currentTarget.files;
        var targetUpload = event.currentTarget;
        this.importedFileName = files[0].name;
        var fileData = new Blob([files[0]], { type: "text/plain" });
        var reader = new FileReader();
        reader.readAsArrayBuffer(fileData);
        reader.addEventListener("loadend", function (e) {
            this.byteArray = new Uint8Array(e.srcElement.result);
            this.callUploadServices(targetUpload);
        }.bind(this));
    };
    TestFormComponent.prototype.callUploadServices = function (targetBtn) {
        var reqObj = {
            'ProjectName': this.project,
            'ReleaseName': this.release,
            'fileName': this.importedFileName
        };
        if (targetBtn.id === "testCreation" && !this.showLastStep) {
            this.showLastStep = true;
        }
    };
    TestFormComponent.prototype.onDownload = function () {
        if (!this.showForm) {
            this.showForm = true;
        }
    };
    TestFormComponent.prototype.onProceed = function () {
        if (!this.triggerFormValidation()) {
            this.showFormError = false;
            this.showTestUpload = true;
            this.showForm = false;
        }
        else {
            this.showFormError = true;
        }
    };
    TestFormComponent.prototype.triggerFormValidation = function () {
        var errCount = 0;
        this.eleArray = Array.from(document.getElementById('formContainer').querySelectorAll('*'));
        this.eleArray.forEach(function (ele) {
            if (['INPUT', 'SELECT'].indexOf(ele.tagName) !== -1 && ele.offsetParent !== null) {
                if (ele.value === '') {
                    errCount++;
                }
            }
        }.bind(this));
        return errCount;
    };
    TestFormComponent.prototype.onSubmit = function () {
        var reqObj = {
            //'NoOftests': this.byteArray,
            'fileName': this.importedFileName,
            'ProjectName': this.project,
            'ReleaseName': this.release,
            'Browser': this.browser,
            'Url': this.url,
            'Snapshot': this.snapshot,
            'SnapshotType': this.snapshotType,
            'HostName': this.host,
            'LocalMachine': this.localMachine
        };
        this.data.postDetails(reqObj);
    };
    ;
    TestFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-test-form',
            template: __webpack_require__(/*! ./test-form.component.html */ "./src/app/test-form/test-form.component.html"),
            styles: [__webpack_require__(/*! ./test-form.component.scss */ "./src/app/test-form/test-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_run_tests_service__WEBPACK_IMPORTED_MODULE_1__["RunTestsService"], _update_navbar_service__WEBPACK_IMPORTED_MODULE_2__["UpdateNavbarService"]])
    ], TestFormComponent);
    return TestFormComponent;
}());



/***/ }),

/***/ "./src/app/test-results/test-results.component.html":
/*!**********************************************************!*\
  !*** ./src/app/test-results/test-results.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  test-results works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/test-results/test-results.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/test-results/test-results.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/test-results/test-results.component.ts":
/*!********************************************************!*\
  !*** ./src/app/test-results/test-results.component.ts ***!
  \********************************************************/
/*! exports provided: TestResultsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestResultsComponent", function() { return TestResultsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TestResultsComponent = /** @class */ (function () {
    function TestResultsComponent() {
    }
    TestResultsComponent.prototype.ngOnInit = function () {
    };
    TestResultsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-test-results',
            template: __webpack_require__(/*! ./test-results.component.html */ "./src/app/test-results/test-results.component.html"),
            styles: [__webpack_require__(/*! ./test-results.component.scss */ "./src/app/test-results/test-results.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TestResultsComponent);
    return TestResultsComponent;
}());



/***/ }),

/***/ "./src/app/update-navbar.service.ts":
/*!******************************************!*\
  !*** ./src/app/update-navbar.service.ts ***!
  \******************************************/
/*! exports provided: UpdateNavbarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateNavbarService", function() { return UpdateNavbarService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UpdateNavbarService = /** @class */ (function () {
    function UpdateNavbarService() {
        this.isRoot = true;
        this.updateNav = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    UpdateNavbarService.prototype.updateNavBar = function (isRoot) {
        this.isRoot = isRoot;
        this.updateNav.emit(this.isRoot);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], UpdateNavbarService.prototype, "updateNav", void 0);
    UpdateNavbarService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], UpdateNavbarService);
    return UpdateNavbarService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\PrazuUI\prazuui\PrazuUI\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map