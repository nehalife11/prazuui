using System;
using Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace PrazuUI.Controllers
{

  [ApiController]
  public class LoginController : Controller
  {
    [Route("api/login")]
    [HttpPost]
    public JsonResult Login([FromBody] Profile profileobj)
    {
      try
      {
        var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
        if (Processor.PrazuProcessor.checkUser(profileobj))
        {
          var profileobject = Processor.PrazuProcessor.GetUserDetails(profileobj);
          if (profileobject != null)
            return Json(new { result = true, data = profileobject, msg = builder.Build()["ForgotPasswordMsgs:LoginSuccessful"] });
          else
            return Json(new { result = false, data = profileobject, msg = builder.Build()["ForgotPasswordMsgs:LoginFailue"] });
        }
        else
        {
          return Json(new { result = false });
        }
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }

    [Route("api/generateOTP")]
    [HttpPost]
    public JsonResult GenerateOTP([FromBody] ForgotPassword reqObj)
    {
      try
      {
        bool result = Processor.PrazuProcessor.generateOTP(reqObj);
        var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
        if (result)
          return Json(new { data = result, msg = builder.Build()["ForgotPasswordMsgs:OTPGenerationSuccess"] });
        else
          return Json(new { data = result, msg = builder.Build()["ForgotPasswordMsgs:OTPGenerationFailure"] });
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }

    [Route("api/passwordresult")]
    [HttpPost]
    public JsonResult ChangePassword([FromBody] ForgotPassword reqObj)
    {
      try
      {
        bool result = Processor.PrazuProcessor.changePassword(reqObj);
        return Json(new { data = result });
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }

    [Route("api/verifyOtp")]
    [HttpPost]
    public JsonResult VerifyOtp([FromBody] ForgotPassword reqObj)
    {
      try
      {
        bool result = Processor.PrazuProcessor.verifyOtp(reqObj);
        var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
        if (result)
          return Json(new { data = result, msg = builder.Build()["ForgotPasswordMsgs:OTPVerificationSuccess"] });
        else
          return Json(new { data = result, msg = builder.Build()["ForgotPasswordMsgs:OTPVerificationFailure"] });
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }
  }
}
