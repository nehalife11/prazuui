using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Mvc;
using ClosedXML.Excel;
using System.Data;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;

namespace PrazuUI.Controllers
{
  [Route("api/form")]
  [Produces("application/json")]
  [ApiController]
  public class FormController : Controller
  {
    // GET api/values
    [HttpGet]
    public ActionResult<IEnumerable<string>> TestGet()
    {
      return new string[] { "value1", "value2" };
    }

    // GET api/form/5
    [HttpGet("{id}")]
    //public JsonResult Get(int id)
    //{
      //var data = GetTestCasesStatus();
      //return new JsonResult(data);
    //}

    public List<DrillDownChart> GetSummary(DataTable dt)
    {
      var finalList = new List<DrillDownChart>();
      foreach (DataRow dr in dt.Rows)
      {
        var item = new DrillDownChart();
        item.Module = dr["Module"].ToString();
        item.TestCasesExecuted = dr["Test Cases Executed"].ToString();
        item.TestCasesPassed = dr["Test Cases Passed"].ToString();
        item.TestCasesFailed = dr["Test Cases Failed"].ToString();
        finalList.Add(item);
      }
      return finalList;
    }


    public FinalObject GetTestCasesStatus(ExecutionManager value,string path = null)
    {
      DataSet passfailData = new DataSet();
      using (XLWorkbook workBook = new XLWorkbook(path))
      {
        foreach (IXLWorksheet workSheet in workBook.Worksheets)
        {
          //Create a new DataTable.
          DataTable dt = new DataTable(workSheet.Name);
          bool firstRow = true;
          foreach (IXLRow row in workSheet.Rows())
          {
            //Use the first row to add columns to DataTable.
            if (firstRow)
            {
              foreach (IXLCell cell in row.Cells())
              {
                dt.Columns.Add(cell.Value.ToString());
              }
              firstRow = false;
            }
            else
            {
              //Add rows to DataTable.
              dt.Rows.Add();
              int i = 0;
              foreach (IXLCell cell in row.Cells(row.FirstCellUsed().Address.ColumnNumber, row.LastCellUsed().Address.ColumnNumber))
              {
                //    foreach (IXLCell cell in row.Cells())
                //{
                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                i++;
              }
            }
            if (!passfailData.Tables.Contains(dt.TableName))
            {
              passfailData.Tables.Add(dt);
            }
          }
        }
      }
      int passcount = 0, failcount = 0;
      Dictionary<string, List<int>> modulePassFail = new Dictionary<string, List<int>>();
      Dictionary<string, int> modulePassPercentage = new Dictionary<string, int>();
      foreach (DataTable dt in passfailData.Tables)
      {
        if (dt != passfailData.Tables[0])
        {
          modulePassFail.Add(dt.TableName, new List<int>());
          //  modulePercentage.Add(dt.TableName, new List<int>());
          int passcountModule = 0, failcountModule = 0;
          foreach (DataRow row in dt.Rows)
          {
            foreach (DataColumn item in row.Table.Columns)
            {
              if (item.ColumnName == "Status")
              {
                if (row["Status"].ToString() == "PASS")
                {
                  passcount = ++passcount;
                  passcountModule = ++passcountModule;
                }
                else
                {
                  failcount = ++failcount;
                  failcountModule = ++failcountModule;
                }
              }
            }
          }
          modulePassFail[dt.TableName].Add(passcountModule);
          modulePassFail[dt.TableName].Add(failcountModule);
          modulePassPercentage[dt.TableName] = Convert.ToInt32(Math.Round(decimal.Divide(passcountModule, (passcountModule + failcountModule)) * 100));
        }
      }
      int passpercent = Convert.ToInt32(Math.Round(decimal.Divide(passcount, (passcount + failcount)) * 100));
      int failpercent = Convert.ToInt32(Math.Round(decimal.Divide(failcount, (passcount + failcount)) * 100));

      var maxModule = modulePassPercentage.Where(s => s.Value == modulePassPercentage.Max(kvp => kvp.Value)).ToList();
      var minModule = modulePassPercentage.Where(s => s.Value == modulePassPercentage.Min(kvp => kvp.Value)).ToList();

      FinalObject finalData = new FinalObject();
      ChartData chartData = new ChartData();
      chartData.summary = new Summary();
      chartData.summary.data = new List<SummaryData>();
      SummaryData summaryData = new SummaryData();
      summaryData.data.Add(passpercent);
      summaryData.data.Add(failpercent);
      chartData.summary.data.Add(summaryData);
      chartData.summary.labels.Add("pass%");
      chartData.summary.labels.Add("fail%");
      chartData.drilldown = new Drilldown();
      List<int> passArray = new List<int>();
      List<int> failArray = new List<int>();
      foreach (string module in modulePassFail.Keys)
      {
        passArray.Add(modulePassFail[module][0]);
        failArray.Add(modulePassFail[module][1]);
        chartData.drilldown.labels.Add(module);
      }
      chartData.drilldown.data.Add(new drilldownData()
      {
        label = "Pass",
        data = passArray
      });
      chartData.drilldown.data.Add(new drilldownData()
      {
        label = "Fail",
        data = failArray
      });
      finalData.chartData = chartData;
      finalData.testCases = new TestCase()
      {
        total = Convert.ToString(passcount + failcount),
        pass = Convert.ToString(passcount),
        fail = Convert.ToString(failcount)
      };
      finalData.testModules = new TestModules();
      finalData.testModules.total = Convert.ToString(modulePassFail.Keys.Count);
      finalData.testModules.highestPass = new PassPercentage();
      for (int i = 0; i < maxModule.Count(); i++)
      {
        if (string.IsNullOrEmpty(finalData.testModules.highestPass.name))
        {
          finalData.testModules.highestPass.name = maxModule[i].Key;
          finalData.testModules.highestPass.count = Convert.ToString(modulePassFail[finalData.testModules.highestPass.name][0]);
        }
        else if (Convert.ToInt32(finalData.testModules.highestPass.count) < maxModule[i].Value)
        {
          finalData.testModules.highestPass.name = maxModule[i].Key;
          finalData.testModules.highestPass.count = Convert.ToString(modulePassFail[finalData.testModules.highestPass.name][0]);
        }
      }
      finalData.testModules.lowestPass = new PassPercentage();
      for (int i = 0; i < minModule.Count(); i++)
      {
        if (string.IsNullOrEmpty(finalData.testModules.lowestPass.name))
        {
          finalData.testModules.lowestPass.name = minModule[i].Key;
          finalData.testModules.lowestPass.count = Convert.ToString(modulePassFail[finalData.testModules.lowestPass.name][0]);
        }
        else if (Convert.ToInt32(finalData.testModules.lowestPass.count) > minModule[i].Value)
        {
          finalData.testModules.lowestPass.name = minModule[i].Key;
          finalData.testModules.lowestPass.count = Convert.ToString(modulePassFail[finalData.testModules.lowestPass.name][0]);
        }
      }

      string baseUrl = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host);

      finalData.pdfUrl = baseUrl + "/assets/documents/" + value.ProjectName + "/" + value.ReleaseName + "/Results/Summary_file.htm";
      return finalData;
    }

    private IHostingEnvironment _hostingEnvironment;
    public FormController(IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
    }
    // POST api/values

    [HttpPost]
    public JsonResult UploadExecutionManager([FromBody] ExecutionManager value)
    {

      string serverUrl = new ConfigurationBuilder().AddJsonFile("appSettings.json").Build()["URL:TomcatServerUrl"];
      DataTable data = new DataTable();
      string webRootPath = _hostingEnvironment.ContentRootPath;
      string filePath = webRootPath + "\\wwwroot\\assets\\documents\\" + value.ProjectName + "\\" + value.ReleaseName;
      string path = filePath + "\\Results\\Results.xlsx";
      string pdfpath = filePath + "\\Results\\OverallExecutionReport.pdf";
      MultipartFormDataContent content = null;
      HttpClient client = null;
      HttpRequestMessage request = null;
      using (client = new HttpClient())
      {
        content = new MultipartFormDataContent();
        request = new HttpRequestMessage(HttpMethod.Post, serverUrl + "services/nooftests");
        content.Add(new StringContent(value.uploadFilesList[0].fileData), "execManagerFIle");
        content.Add(new StringContent(value.ProjectName), "projectName");
        content.Add(new StringContent(value.ReleaseName), "release");
        content.Add(new StringContent(value.LocalMachine), "executionMachine");
        content.Add(new StringContent(filePath), "filePath");
        request.Content = content;
        request.Content.Headers.Add("ContentType", "multipart/form-data");
        HttpResponseMessage response = client.SendAsync(request).Result;
        value.uploadFilesList.Remove(value.uploadFilesList[0]);
      }

      UploadFile file = value.uploadFilesList.Where(x => x.fileName.Contains("salesforce")).FirstOrDefault();
      if (file != null)
      {
        using (client = new HttpClient())
        {
          content = new MultipartFormDataContent();
          request = new HttpRequestMessage(HttpMethod.Post, serverUrl + "services/uploadObjectMapping");
          content.Add(new StringContent(file.fileData), "objectMappingFIle");
          content.Add(new StringContent(value.ProjectName), "projectName");
          content.Add(new StringContent(value.ReleaseName), "release");
          content.Add(new StringContent(value.LocalMachine), "executionMachine");
          content.Add(new StringContent(filePath), "filePath");
          request.Content = content;
          request.Content.Headers.Add("ContentType", "multipart/form-data");
          HttpResponseMessage response = client.SendAsync(request).Result;
          value.uploadFilesList.Remove(file);
        }
      }

      foreach(UploadFile singlefile in value.uploadFilesList)
      {
        using (client = new HttpClient())
        {
          content = new MultipartFormDataContent();
          request = new HttpRequestMessage(HttpMethod.Post, serverUrl+ "services/uploadModules");
          content.Add(new StringContent(singlefile.fileData), "moduleFIle");
          content.Add(new StringContent(singlefile.fileName), "fileName");
          content.Add(new StringContent(value.ProjectName), "projectName");
          content.Add(new StringContent(value.ReleaseName), "release");
          content.Add(new StringContent(value.LocalMachine), "executionMachine");
          content.Add(new StringContent(filePath), "filePath");
          request.Content = content;
          request.Content.Headers.Add("ContentType", "multipart/form-data");
          HttpResponseMessage response = client.SendAsync(request).Result;
        }
      }
      
      using (client = new HttpClient())
      {
        content = new MultipartFormDataContent();
        request = new HttpRequestMessage(HttpMethod.Post, serverUrl + "services/automation");
        content.Add(new StringContent(value.Browser), "browser");
        content.Add(new StringContent(value.Url), "url");
        content.Add(new StringContent(value.Snapshot), "snapshotType");
        content.Add(new StringContent(value.ProjectName), "projectName");
        content.Add(new StringContent(value.ReleaseName), "release");
        content.Add(new StringContent(value.snapshotForAllPass), "snapshotForAllPass");
        content.Add(new StringContent(value.HostName), "hostName");
        content.Add(new StringContent(filePath), "filepath");
        content.Add(new StringContent(value.LocalMachine), "executionMachine");
        client.Timeout = Timeout.InfiniteTimeSpan;
        request.Content = content;
        request.Content.Headers.Add("ContentType", "multipart/form-data");
        HttpResponseMessage response = client.SendAsync(request).Result;
      }
      
      using (XLWorkbook workBook = new XLWorkbook(path))
      {
        IXLWorksheet workSheet = workBook.Worksheet(1);

        //Create a new DataTable.
        DataTable dt = new DataTable();

        //Loop through the Worksheet rows.
        bool firstRow = true;
        foreach (IXLRow row in workSheet.Rows())

        {
          //Use the first row to add columns to DataTable.
          if (firstRow)
          {
            foreach (IXLCell cell in row.Cells())
            {
              dt.Columns.Add(cell.Value.ToString());
            }
            firstRow = false;
          }
          else
          {
            //Add rows to DataTable.
            dt.Rows.Add();
            int i = 0;
            foreach (IXLCell cell in row.Cells(row.FirstCellUsed().Address.ColumnNumber, row.LastCellUsed().Address.ColumnNumber))
            {
              //    foreach (IXLCell cell in row.Cells())
              //{
              dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
              i++;
            }
          }

        }
        data = dt;
      }
      var abc = GetTestCasesStatus(value, path);

      var xyz = GetSummary(data);
      return Json(new { success = true, summaryChart = abc, drillDownChart = xyz});
    }

    [HttpGet]
    [Route("GetHostName")]
    public JsonResult GetLocalIPAddress()
    {
      var host = Dns.GetHostEntry(Dns.GetHostName());
      foreach (var ip in host.AddressList)
      {
        if (ip.AddressFamily == AddressFamily.InterNetwork)
        {
          return new JsonResult(ip.ToString());
        }
      }
      throw new Exception("No network adapters with an IPv4 address in the system!");
    }
  }
}



public class TestStatus
{
  public int value;
  public string label;
}

public class DrillDownChart
{
  public string Module { get; set; }
  public string TestCasesPassed { get; set; }
  public string TestCasesFailed { get; set; }
  public string TestCasesExecuted { get; set; }

}

public class ChartData
{
  public Summary summary;
  public Drilldown drilldown;

}

public class Summary
{
  public List<SummaryData> data;
  public List<string> labels = new List<string>();
}

public class SummaryData
{
  public List<int> data = new List<int>();
}

public class drilldownData
{
  public string label { get; set; }
  public List<int> data = new List<int>();
}

public class Drilldown
{
  public List<drilldownData> data = new List<drilldownData>();
  public List<string> labels = new List<string>();
}

public class FinalObject
{
  public ChartData chartData;
  public TestCase testCases;
  public TestModules testModules;
  public string pdfUrl { get; set; }
}

public class TestCase
{
  public string total { get; set; }
  public string pass { get; set; }
  public string fail { get; set; }
}

public class TestModules
{
  public string total { get; set; }
  public PassPercentage highestPass;
  public PassPercentage lowestPass;
}

public class PassPercentage
{
  public string name { get; set; }
  public string count { get; set; }
}

