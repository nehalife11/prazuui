using System;
using System.Data;
using Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace PrazuUI.Controllers
{

  [ApiController]
  public class RegisterController : Controller
  {
    [Route("api/register")]
    [HttpPost]
    public JsonResult Register([FromBody] Profile profileobj)
    {
      try
      {
        string querytoInsert = Processor.Query.QueryToInsertUser;
        var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
        var Configuration = builder.Build();
        var secretKey = Configuration["ConnectionStrings:secretKey"];
        querytoInsert = querytoInsert + "('" + profileobj.UserMap.UserName + "',AES_ENCRYPT('" + profileobj.UserMap.UserPassword + "', '" + secretKey + "'),'" + profileobj.UserMap.UserEmail + "')";
        int userId = 0;
        var result = Processor.PrazuProcessor.insertintoUserMaster(querytoInsert, profileobj);
        if (result)
        {
          userId = Processor.PrazuProcessor.retrieveUserId();
        }
        else
        {
          return new JsonResult("UserName exists");
        }
        if (userId > 0)
        {
          string querytoInsertProfile = Processor.Query.QueryToInsertProfile;
          querytoInsertProfile = querytoInsertProfile + "(" + userId + "," + profileobj.RoleMap.RoleID + "," + profileobj.ProjectMap.ProjectId + ")";
          var profileresult = Processor.PrazuProcessor.insertintoDB(querytoInsertProfile);
          return Json(new { isReference = false, data = profileresult });
        }
        return null;
      }
      catch (Exception ce)
      {
        throw ce;
      }
    }
    [Route("api/populatedropdown")]
    [HttpPost]
    [HttpGet]
    public JsonResult PopulateDropDownsFromDB()
    {
      var dropdownsList = Processor.PrazuProcessor.GetDataForDropDown();
      DataTable dataTable = new DataTable();
      return Json(new { isReference = true, RoleDropDown = dropdownsList[0], ProjectDropDown = dropdownsList[1] });

    }
  }
}
