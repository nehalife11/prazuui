import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../login-service.service';
import { SpinnerServiceService } from '../spinner-service.service';
import { UpdateNavbarService } from '../update-navbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {

  constructor(private data: LoginServiceService, private spinnerService: SpinnerServiceService, private router: Router, private navBarService: UpdateNavbarService) { }

  userEmail: string = '';
  showStatusMsg: boolean = false;
  requestFailed: boolean = false;
  statusMsg: string = "";
  otp: string = '';
  newPassword: string = '';
  reEnterPassword: string = '';
  passwordMatched: boolean = true;
  isEmpty: boolean = true;
  isDisabled: boolean = true;
  manageSections = {
    'enterEmail' : true,
    'enterOtp': false,
    'enterPassword': false
  };
  messageType: string = '';

  ngOnInit() {
    this.showStatusMsg = false;
    this.manageSections = {
      'enterEmail' : true,
      'enterOtp': false,
      'enterPassword': false
    };
    this.navBarService.updateNavBar(true, '/forgot-password');
    this.data.validate.subscribe(isError => {
      this.requestFailed = isError;
    });
    this.data.response.subscribe(options => {
      this.statusMsg = options[0].msg;
      this.manageSections = options[1];
      this.showStatusMsg = options[2];
      this.messageType = options[3];
    });
  }

  checkSame(secPswd: string) {
    this.reEnterPassword = secPswd;
    this.passwordMatched = true;
    this.isDisabled = true;
    if(this.reEnterPassword.length === 0) {
      this.isEmpty = true;
    } else {
        if(this.reEnterPassword === this.newPassword) {
        this.isDisabled = false;
      } else {
        this.passwordMatched = false;
      }
    }
  }

  checkEmpty(frstPswd: string) {
    this.newPassword = frstPswd;
    this.passwordMatched = true;
    this.isDisabled = true;
    if(this.newPassword.length === 0 && this.reEnterPassword.length > 0) {
      this.isDisabled = true;
      this.reEnterPassword = '';
    } else if(this.reEnterPassword.length !== 0) {
      if(this.newPassword === this.reEnterPassword) {
        this.isDisabled = false;
      } else {
        this.passwordMatched = false;
      }
    }
  }

  getOtp() {
    var reqObj = {
      'userEmail': this.userEmail
    };
    this.data.getOtp(reqObj);
    // this.spinnerService.displaySpinner(true);
    // this.mockEmailValidation();
  }
  mockEmailValidation() {
    this.spinnerService.displaySpinner(false);
    if(this.userEmail === 'iproy@deloitte.com') {
      this.showStatusMsg = true;
      this.statusMsg = "OTP is sent to your registered e-mail address successfully.";
      this.manageSections = {
        'enterEmail' : false,
        'enterOtp': true,
        'enterPassword': false
      };
      this.messageType = 'success';
    }
    else {
      this.showStatusMsg = true;
      this.statusMsg = "OTP couldn't be sent to your e-mail address.";
      this.messageType = 'error';
    }
  }

  verifyOtp() {
    var reqObj = {
      'otp': this.otp,
      'userEmail': this.userEmail
    };
    this.data.verifyOtp(reqObj);
    // this.spinnerService.displaySpinner(true);
    // this.mockOtpValidation();
  }

  mockOtpValidation() {
    this.spinnerService.displaySpinner(false);
    if(this.otp === '12345') {
      this.showStatusMsg = false;
      this.manageSections = {
        'enterEmail' : false,
        'enterOtp': false,
        'enterPassword': true
      };
    } else {
      this.showStatusMsg = true;
      this.statusMsg = "The OTP entered is incorrect."
    }
  }

  changePassword() {
    var reqObj = {
      'userEmail': this.userEmail,
      'passWord': this.newPassword
    };
    this.data.updatePassword(reqObj);
    // this.spinnerService.displaySpinner(true);
    // this.mockUpdatePassword();
  }

  mockUpdatePassword() {
    this.spinnerService.displaySpinner(false);
    this.router.navigateByUrl('/');
  }

}
