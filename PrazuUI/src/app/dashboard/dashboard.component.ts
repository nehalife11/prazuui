import { Component, OnInit } from '@angular/core';
import { UpdateNavbarService } from '../update-navbar.service';
import { SpinnerServiceService } from '../spinner-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private navBarService: UpdateNavbarService,  private spinnerService: SpinnerServiceService) { }

  executedProjects = [{
  	project: 'Prazu',
  	category: 'Salesforce',
  	completed_on: '3 Dec',
  	modules: 10,
  	pass: 100,
  	fail: 100,
  	time_taken: '5 Min'
  }, {
  	project: 'Stericycle',
  	category: 'Hybris',
  	completed_on: '3 Dec',
  	modules: 20,
  	pass: 10,
  	fail: 10,
  	time_taken: '10 Min'
  }, {
  	project: 'Qantas',
  	category: 'AEM',
  	completed_on: '2 Dec',
  	modules: 20,
  	pass: 60,
  	fail: 60,
  	time_taken: '2 Min'
  }, {
  	project: 'Bose',
  	category: 'Drupal',
  	completed_on: '1 Dec',
  	modules: 30,
  	pass: 80,
  	fail: 80,
  	time_taken: '20 Min'
  }, {
  	project: 'BMW',
  	category: '.Net',
  	completed_on: '30 Nov',
  	modules: 10,
  	pass: 200,
  	fail: 200,
  	time_taken: '1 Min'
  }, {
  	project: 'Apple',
  	category: 'JAVA',
  	completed_on: '29 Nov',
  	modules: 12,
  	pass: 231,
  	fail: 231,
  	time_taken: '3 Min'
  }]

  clientFeedbackChartData = [{data: [40, 30, 10, 20]}];
  clientFeedbackChartLabels = ['Strongly Agree', 'Agree', 'Neutral', 'Poor'];
  clientFeedbackChartOptions = {
  	responsive: true,
	  maintainAspectRatio: false,
	  legend: {
	    display: true,
	    position: 'right',
	    fullWidth: true,
	    labels: {
	      fontSize: 12,
	      fontColor: '#4A4A4A',
	      padding: 20,
	      boxWidth: 40
	    }
    }
  };
  clientFeedbackChartColors = [{
  	backgroundColor: ['#0076A8', '#01ABAB', '#ED8B00', '#D8281B'],
  	borderWidth: 0
  }];

  projectsTrendChartData = [{
  	data: [35, 40, 67, 10, 56, 97, 89, 50, 32, 88, 45, 77],
  	label: 'Salesforce'
  }, {
  	data: [68, 87, 45, 20, 100, 56, 43, 12, 98, 48, 86, 36],
  	label: 'Other'
  }];

  projectsTrendChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  projectsTrendChartOptions = {
  	responsive: true,
	  maintainAspectRatio: false,
	  legend: {
	    display: true,
	    position: 'top',
	    fullWidth: true,
	    labels: {
	      fontSize: 12,
	      fontColor: '#4A4A4A',
	      padding: 20,
	      boxWidth: 40
	    }
    }
  };
  projectsTrendChartColors = [{
  	backgroundColor: '#0076A8'
  }, {
  	backgroundColor: '#30B9BA'
  }]

  ngOnInit() {
		this.navBarService.updateNavBar(false, '/dashboard');
  }

}
