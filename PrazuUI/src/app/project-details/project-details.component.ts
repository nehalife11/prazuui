import { Component, OnInit, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { RunTestsService } from '../run-tests.service';
import { UpdateNavbarService } from '../update-navbar.service';
import { SpinnerServiceService } from '../spinner-service.service';
// import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  activeTab: any = '';

  projectInfoSubmitted = false;
  executionManagerUploaded = false;
  executionManagerSubmitted = false;
  testCreationUploaded = false;
  testCreationSubmitted = false;
  executionManagerFiles = [];
  testCreationFiles = [];
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  reqObj: object = {};
  byteArray: any[];
  finalByteArray: any[];
  uploadFilesCollection = [];
  fileDetailObj: object = {};
  baseFile: string;

  @ViewChild('fileInput') fileInput: ElementRef;
  constructor(private navBarService: UpdateNavbarService, private spinnerService: SpinnerServiceService, private data: RunTestsService) {
    // this.options = { concurrency: 1, maxUploads: 1 };
    this.executionManagerFiles = []; // local uploading files array
    this.testCreationFiles = [];
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  browsers = [{
    data: 'Chrome',
    val: 'Chrome'
  }, {
    data: 'Firefox',
    val: 'firefox'
  }, {
    data: 'IE',
    val: 'ie'
  }];

  snapshots = [{
    data: 'Browser ',
    val: 'browser'
  }, {
    data: 'Desktop',
    val: 'desktop'
  }];

  snapshotTypes = [{
    data: 'Yes',
    val: 'yes'
  }, {
    data: 'No',
    val: 'no'
  }];

  hostNames = [{
    data: 'Host 1',
    val: 'host1'
  }, {
    data: 'Host 2',
    val: 'host2'
  }, {
    data: 'Host 3',
    val: 'host3'
  }];

  localMachines = [{
    data: 'Local Machine',
    val: 'local machine'
  }, {
    data: 'Server',
    val: 'server'
  }];

  defaultValues = {
    browser: 'default', snapshot: 'default', snapshotType: 'default', hostName: '',
    localMachine: 'default', projectName: '', releaseName: '', projectUrl: ''
  };

  projectInfoFormSubmit() {
    this.projectInfoSubmitted = true;
    this.activeTab = 'tab2';
  }

  executionManagerSubmit() {
    this.executionManagerSubmitted = true;
    this.activeTab = 'tab3';
  }

  testCreationSubmit() {
    this.testCreationSubmitted = true;
    this.startUpload();

  }

  activateTab(tabNo) {
    if (tabNo === 'tab2') {
      if (this.projectInfoSubmitted) {
        this.activeTab = tabNo;
      }
    } else if (tabNo === 'tab3') {
      if(this.executionManagerFiles.length == 0){
        return;
      }
      if (this.executionManagerSubmitted) {
        this.activeTab = tabNo;
      }
    } else {
      this.activeTab = tabNo;
    }
  }

  // public files: UploadFile[] = [];

  onUploadOutput(output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        // uncomment this if you want to auto upload files when added
        // const event: UploadInput = {
        //    type: 'uploadAll',
        //    url: '/upload',
        //    method: 'POST',
        //    data: { foo: 'bar' }
        //  };
        // this.uploadInput.emit(event);
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {
          this.fileDetailObj = output.file;
          var fileData = new Blob([this.fileDetailObj[0]], { type: "text/plain" });
          var reader = new FileReader();
          reader.readAsArrayBuffer(fileData);

          reader.addEventListener("loadend", function (e) {
            debugger;
            this.byteArray = new Uint8Array(e.srcElement.result);
            let obj = {
              name: output.file.name,
              data: this.baseFile
            };
            this.uploadFilesCollection.push(obj);
            if (this.activeTab === 'tab2') {
              this.executionManagerFiles.push(this.fileDetailObj);
              this.executionManagerUploaded = true;
            } else if (this.activeTab === 'tab3') {
              this.testCreationFiles.push(this.fileDetailObj);
              this.testCreationUploaded = true;
            }
          }.bind(this));

        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          if (this.activeTab === 'tab2') {
            const index = this.executionManagerFiles.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.executionManagerFiles[index] = output.file;
          } else if (this.activeTab === 'tab3') {
            const index = this.testCreationFiles.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.testCreationFiles[index] = output.file;
          }
        }
        break;
      case 'removed':
        // remove file from array when removed
        if (this.activeTab === 'tab2') {
          this.executionManagerFiles = this.executionManagerFiles.filter((file: UploadFile) => file !== output.file);
        } else if (this.activeTab === 'tab3') {
          this.testCreationFiles = this.testCreationFiles.filter((file: UploadFile) => file !== output.file);
        }
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
    }
  }

  ab2str(arrayBuffer) {
    return window.btoa(String.fromCharCode.apply(null, new Uint8Array(arrayBuffer)));
  }

  startUpload(): void {
    console.log('{foo: "bar", projectName: ' + this.defaultValues.projectName + ',releaseName: '
      + this.defaultValues.releaseName + ',browsers: ' + this.defaultValues.browser + '}');
    const reqObj = {
      ProjectName: this.defaultValues.projectName,
      ReleaseName: this.defaultValues.releaseName,
      Browser: this.defaultValues.browser,
      Url: this.defaultValues.projectUrl,
      snapshot: this.defaultValues.snapshot,
      snapshotForAllPass: this.defaultValues.snapshotType,
      hostName: this.defaultValues.hostName,
      localMachine: this.defaultValues.localMachine,
      uploadFilesList: this.uploadFilesCollection
    };
    this.spinnerService.displaySpinner(true);
    this.data.postDetails(reqObj);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: any): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  ngOnInit() {
    this.navBarService.updateNavBar(false, '/project-details');
    this.activeTab = 'tab1';
    this.data.getHostName();
    this.data.getData.subscribe(options => {
      this.defaultValues.hostName = options
    });
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
    //  reader.readAsArrayBuffer(fileData);
      reader.addEventListener("loadend", function (e) {
        this.baseFile = (e.srcElement.result as string).split(',')[1]
        //  reader.onload = () => {
        //    this.baseFile = (reader.result as string).split(',')[1]
        //    };
        //}
        let obj = {
          fileName: event.target.files[0].name,
          fileData: this.baseFile
        };
         if(obj.fileName == "ExecutionManager.xlsx") {
          this.uploadFilesCollection.unshift(obj);
         }
         else {
          this.uploadFilesCollection.push(obj);
         }
        if (this.activeTab === 'tab2') {
          this.executionManagerFiles.push(event.target.files[0]);
          this.executionManagerUploaded = true;
        } else if (this.activeTab === 'tab3') {
          this.testCreationFiles.push(event.target.files[0]);
          this.testCreationUploaded = true;
        }
      }.bind(this));

    }
  }
  onRemoveTestCreation(name) {
    this.testCreationFiles.forEach(function(entry,index){
      if(entry.name==name){
        this.testCreationFiles.splice(index,1);
      }
    }.bind(this));
    if(this.testCreationFiles.length == 0){
      this.testCreationUploaded = false;
    }
    this.uploadFilesCollection.forEach(function(entry,index){
      if(entry.fileName==name){
        this.uploadFilesCollection.splice(index,1);
      }
    }.bind(this));
  }
  onRemoveExecutionManager(name) {
    this.executionManagerFiles = [];
    if(this.executionManagerFiles.length == 0){
      this.executionManagerUploaded = false;
    }
    this.uploadFilesCollection.forEach(function(entry,index){
      if(entry.fileName==name){
        this.uploadFilesCollection.splice(index,1);
      }
    }.bind(this));
  }
}
