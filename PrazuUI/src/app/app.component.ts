import { Component } from '@angular/core';
import { SpinnerServiceService } from './spinner-service.service';
import { UpdateNavbarService } from './update-navbar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private spinnerService: SpinnerServiceService, private navBarService: UpdateNavbarService) { }

  hideSpinner = true;
  isRoot: boolean = true;

  ngOnInit() {
    this.spinnerService.change.subscribe(showSpinner => {
      this.hideSpinner = !showSpinner;
    });
    this.navBarService.updateNav.subscribe(data => {
      this.isRoot = data.isRoot;
    });
  }

}
