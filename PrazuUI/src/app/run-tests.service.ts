import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SpinnerServiceService } from './spinner-service.service';

@Injectable({
  providedIn: 'root'
})
export class RunTestsService {

  constructor(private http: HttpClient, private router: Router, private spinnerService: SpinnerServiceService) {}

  responseObj: any = {};
  @Output() getData: EventEmitter<any> = new EventEmitter();

  postDetails(requestObject) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/Json' })
    };
    return this.http.post('api/form', requestObject, httpOptions).subscribe(result => {
        console.log("Result is:",result);
        this.responseObj = result;
        this.spinnerService.displaySpinner(false);
        this.router.navigateByUrl('/results');
      }, error => {
        this.spinnerService.displaySpinner(false);    
        // this.responseObj = {
        //   chartData: {
        //       summary: {
        //           data: [{
        //               data: [60, 40],
        //           }],
        //           labels: ['Pass %', 'Fail %']
        //       },
        //       drilldown: {
        //           data: [{
        //               label: 'Pass',
        //               data: [1, 2, 3, 4, 5]
        //           }, {
        //               label: 'Fail',
        //               data: [9, 8, 7, 6, 5]
        //           }],
        //           labels: ['Module1', 'Module2', 'Module3', 'Module4', 'Module5']
        //       }
        //   },
        //   testCases: {
        //       total: '100',
        //       pass: '75',
        //       fail: '25'
        //   },
        //   testModules: {
        //       total: '6',
        //       highestPass: {
        //           name: 'Login',
        //           count: '59'
        //       },
        //       lowestPass: {
        //           name: 'signout',
        //           count: '9'
        //       }
        //   }
        // };
        // this.router.navigateByUrl('/results');
      });
  }

  getHostName() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/Json' })
    };
    return this.http.get('api/form/GetHostName', httpOptions).subscribe(result => {
      this.responseObj = result;
      this.getData.emit(this.responseObj);
    });
  }
}
