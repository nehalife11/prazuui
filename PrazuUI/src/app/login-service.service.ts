import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SpinnerServiceService } from './spinner-service.service';
import { UpdateNavbarService } from './update-navbar.service';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(private http: HttpClient, private router: Router, private spinnerService: SpinnerServiceService, private navBarService: UpdateNavbarService) { }

  responseObj: any = {};
  manageSections: any = {};
  showMessage: boolean = false;
  messageType: string = '';

  @Output() validate: EventEmitter<any> = new EventEmitter();
  @Output() response: EventEmitter<any> = new EventEmitter();
  @Output() loginResponse: EventEmitter<any> = new EventEmitter();

  fireRequest(requestObject) {
    this.validate.emit(false);
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'text/json' })
    };
    return this.http.post('api/login', requestObject, httpOptions).subscribe(result => {
        this.spinnerService.displaySpinner(false);
        this.responseObj = result;
        if(this.responseObj.result && this.responseObj.data) {
          if(this.responseObj.data.roleMap.roleName === 'PortfolioLead'){
            this.router.navigateByUrl('/dashboard');
            this.navBarService.setUserData(this.responseObj.data.userMap.userName, 'lead');
          }
          else {
            this.router.navigateByUrl('/project-details');
            this.navBarService.setUserData(this.responseObj.data.userMap.userName, 'user');
          }
        } else {
          this.showMessage = true;
          this.messageType = 'error';
          this.loginResponse.emit([this.responseObj, this.showMessage, this.messageType]);
        }
      }, error => {
        this.spinnerService.displaySpinner(false);
        this.validate.emit(true);
         //this.navBarService.setUserData('Ravi', 'lead1');
         //this.router.navigateByUrl('/project-details');
        // this.router.navigateByUrl('/dashboard');

    });
  }

  getOtp(reqObj) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'text/json' })
    };
    return this.http.post('api/generateOTP', reqObj, httpOptions).subscribe(result => {
      this.spinnerService.displaySpinner(false);
      this.responseObj = result;
      if (this.responseObj.data) {
        this.manageSections = {
          'enterEmail': false,
          'enterOtp': true,
          'enterPassword': false
        };
        this.messageType = 'success';
      } else {
        this.manageSections = {
          'enterEmail': true,
          'enterOtp': false,
          'enterPassword': false
        };
        this.messageType= 'error';
      }
      this.showMessage = true;
      this.response.emit([this.responseObj, this.manageSections, this.showMessage, this.messageType]);
    }, error => {
      this.spinnerService.displaySpinner(false);
      this.validate.emit(true);

    });
  }

  verifyOtp(reqObj) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'text/json' })
    };
    return this.http.post('api/verifyOtp', reqObj, httpOptions).subscribe(result => {
      this.spinnerService.displaySpinner(false);
      this.responseObj = result;
      if(this.responseObj.data) {
        this.manageSections = {
          'enterEmail' : false,
          'enterOtp': false,
          'enterPassword': true
        };
        this.showMessage = false;
        this.messageType = 'success';
      } else {
        this.manageSections = {
          'enterEmail' : false,
          'enterOtp': true,
          'enterPassword': false
        };
        this.showMessage = true;
        this.messageType = 'error';
      }
      this.response.emit([this.responseObj, this.manageSections, this.showMessage, this.messageType]);
    }, error => {
      this.spinnerService.displaySpinner(false);
      this.validate.emit(true);

    });
  }

  updatePassword(reqObj) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'text/json' })
    };
    return this.http.post('api/passwordresult', reqObj, httpOptions).subscribe(result => {
      this.spinnerService.displaySpinner(false);
      this.responseObj = result;
      if(this.responseObj.data) {
        this.showMessage = true;
        this.messageType = 'success';
        this.loginResponse.emit([this.responseObj, this.manageSections, this.showMessage, this.messageType]);
        this.router.navigateByUrl('/');
      } else {
        this.showMessage = true;
        this.manageSections = {
          'enterEmail' : false,
          'enterOtp': false,
          'enterPassword': true
        };
        this.messageType= 'error';
        this.response.emit([this.responseObj, this.manageSections, this.showMessage, this.messageType]);
      }
    }, error => {
      this.spinnerService.displaySpinner(false);
      this.validate.emit(true);

    });
  }
}
