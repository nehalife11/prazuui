import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TestFormComponent } from './test-form/test-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { GlobalSpinnerComponent } from './global-spinner/global-spinner.component';
import { TestResultsComponent } from './test-results/test-results.component';
import { TooltipModule } from 'ngx-tooltip';
import { jqxChartComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChartsModule } from 'ng2-charts';
import { FileDropModule } from 'ngx-file-drop';
import { NgxUploaderModule } from 'ngx-uploader';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    TestFormComponent,
    FooterComponent,
    GlobalSpinnerComponent,
    TestResultsComponent,
    jqxChartComponent,
    ProjectDetailsComponent,
    ResetPasswordComponent,
    DashboardComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TooltipModule,
    ChartsModule,
    FileDropModule,
    NgxUploaderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
