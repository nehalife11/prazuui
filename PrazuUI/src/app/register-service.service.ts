import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SpinnerServiceService } from './spinner-service.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterServiceService {

  constructor(private http: HttpClient, private router: Router, private spinnerService: SpinnerServiceService) { }
  responseObj: any = {};
  registerOptionsForRole: any[];
  registerOptionsForProjects: any[];
  @Output() validate: EventEmitter<any> = new EventEmitter();
  @Output() update: EventEmitter<any> = new EventEmitter();

  fireRequest(requestObject,forRequest) {
    let reqUrl = '';
    if(forRequest === 'submit'){
      reqUrl = 'api/register';
    }
    if(forRequest === 'getReference'){
      reqUrl = 'api/populatedropdown';
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'text/json' })
    };
    return this.http.post(reqUrl, requestObject, httpOptions).subscribe(result => {
        this.responseObj = result;
        if(this.responseObj.isReference) {
          this.registerOptionsForRole = this.responseObj.roleDropDown;
          this.registerOptionsForProjects = this.responseObj.projectDropDown;
          this.update.emit([this.registerOptionsForRole, this.registerOptionsForProjects]);
          this.spinnerService.displaySpinner(false);
        }
        else {
          this.spinnerService.displaySpinner(false);
          this.router.navigateByUrl('/');
        }
      }, error => {
        this.spinnerService.displaySpinner(false);
        this.validate.emit(true);
      });
  }
}
