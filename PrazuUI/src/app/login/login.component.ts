import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../login-service.service';
import { SpinnerServiceService } from '../spinner-service.service';
import { UpdateNavbarService } from '../update-navbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(private data: LoginServiceService, private spinnerService: SpinnerServiceService, private router: Router, private navBarService: UpdateNavbarService) { }

  userName: string = '';
  password: string = '';
  showError: boolean = false;
  requestFailed: boolean = false;
  showStatusMsg: boolean = false;
  messageType: string = '';
  statusMsg: string = '';

  ngOnInit() {
    this.navBarService.updateNavBar(true, '/');
    this.data.validate.subscribe(isError => {
      this.requestFailed = isError;
    });
    this.data.loginResponse.subscribe(options => {
      this.statusMsg = options[0].msg;
      this.showStatusMsg = options[1];
      this.messageType = options[2];
    });
  }

  handleLogin() {
      var reqObj = {
        UserMap: {
          UserName: this.userName,
          UserPassword: this.password
        }
    };
      this.spinnerService.displaySpinner(true);
      this.data.fireRequest(reqObj);
      //this.mockLoginValidation();
    }
    mockLoginValidation() {
      if(this.userName === 'user' && this.password === 'password') {
        this.showError = false;
        this.router.navigateByUrl('/project-details');
        //this.spinnerService.displaySpinner(false);
      }
      else {
        this.showError = true;
      }
    }

  }
