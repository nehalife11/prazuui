import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../register-service.service';
import { SpinnerServiceService } from '../spinner-service.service';
import { UpdateNavbarService } from '../update-navbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private data: RegisterServiceService, private spinnerService: SpinnerServiceService , private router: Router, private navBarService: UpdateNavbarService) { }

  firstName: string = '';
  lastName: string = '';
  roleName: string = 'default';
  projectName: string = 'default';
  email: string = '';
  password: string = '';
  confirmPasswords: string = '';
  showError: boolean = false;
  roles: any[];
  projects: any[];
  reqObj: object;
  passwordMatched: boolean = true;
  isEmpty: boolean = true;
  isDisabled: boolean = true;
  requestFailed: boolean = false;

  ngOnInit() {
    this.navBarService.updateNavBar(true, '/register');
    this.data.fireRequest({},'getReference');
    this.data.update.subscribe(options => {
      this.roles = options[0];
      this.projects = options[1];
    });
    this.data.validate.subscribe(isError => {
      this.requestFailed = isError;
    });
  }

  handleRegister() {
    this.reqObj = {
      UserMap: {
        UserName: this.firstName,
        UserPassword: this.password,
        UserEmail: this.email
      },
      RoleMap: {
        RoleID: this.roleName
      },
      ProjectMap: {
        ProjectId: this.projectName
      }
    };
    this.spinnerService.displaySpinner(true);
    this.data.fireRequest(this.reqObj, 'submit');
  }

  checkSame(secPswd: string) {
    this.confirmPasswords = secPswd;
    this.passwordMatched = true;
    this.isDisabled = true;
    if(this.confirmPasswords.length === 0) {
      this.isEmpty = true;
    } else {
        if(this.confirmPasswords === this.password) {
        this.isDisabled = false;
      } else {
        this.passwordMatched = false;
      }
    }
  }

  checkEmpty(frstPswd: string) {
    this.password = frstPswd;
    this.passwordMatched = true;
    this.isDisabled = true;
    if(this.password.length === 0 && this.confirmPasswords.length > 0) {
      this.isDisabled = true;
      this.confirmPasswords = '';
    } else if(this.confirmPasswords.length !== 0) {
      if(this.password === this.confirmPasswords) {
        this.isDisabled = false;
      } else {
        this.passwordMatched = false;
      }
    }
  }

}
