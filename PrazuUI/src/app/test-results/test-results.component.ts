import { Component, OnInit } from '@angular/core';
import { RunTestsService } from '../run-tests.service';
import { UpdateNavbarService } from '../update-navbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test-results',
  templateUrl: './test-results.component.html',
  styleUrls: ['./test-results.component.scss']
})
export class TestResultsComponent implements OnInit {
  
    onChartClick: any;
    summaryChartData: any = [];
    drilldownChartData: any = [];
    summaryChartLabels: any = [];
    drilldownChartLabels: any = [];
    testCasesSummary: any = {};
    testModulesSummary: any = {};
    pdfUrl: any;
    summaryChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right',
        fullWidth: true,
        labels: {
          fontSize: 12,
          fontColor: '#4A4A4A',
          padding: 40,
          boxWidth: 50
        }
    }
    };

    summaryChartColors = [{
      backgroundColor: ['#0076A8', '#01ABAB'],
    }];

    drilldownChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      barThickness: 2,
      legend: {
        display: true,
        position: 'right',
        fullWidth: false,
        labels: {
          fontSize: 12,
          fontColor: '#4A4A4A',
          padding: 40,
          boxWidth: 30
        }
      },
      scales: {
        xAxes: [{
            stacked: true,
            barThickness: 20,
            gridLines: {
              display:false
            }
        }],
        yAxes: [{
            stacked: true,
            barThickness: 20,
            gridLines: {
              display:false
            }
        }]
    }
    };

    drilldownChartColors = [{
      backgroundColor: '#0076A8'
      },
      {
        backgroundColor: '#01ABAB'
      }];

  constructor( private testsService: RunTestsService, private navBarService: UpdateNavbarService, private router: Router) { }

  ngOnInit() {
    this.navBarService.updateNavBar(false, '/results');
    let chartData = this.testsService.responseObj.summaryChart.chartData;
    let summaryChart = chartData.summary;
    let drilldownchart = chartData.drilldown;
    this.summaryChartData = summaryChart.data;
    this.drilldownChartData = drilldownchart.data;
    this.summaryChartLabels = summaryChart.labels;
    this.drilldownChartLabels = drilldownchart.labels;
    this.testCasesSummary = this.testsService.responseObj.summaryChart.testCases;
    this.testModulesSummary = this.testsService.responseObj.summaryChart.testModules;
    this.pdfUrl = this.testsService.responseObj.summaryChart.pdfUrl;
  }

  navigateToDashboard() {
      this.router.navigateByUrl('/form');
  }
}

