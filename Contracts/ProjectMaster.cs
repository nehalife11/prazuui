﻿namespace Contracts
{
    public class ProjectMaster
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectSPOC { get; set; }
        public string ProjectType { get; set; }
    }
}
