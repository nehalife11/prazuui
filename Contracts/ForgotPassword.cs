﻿namespace Contracts
{
    public class ForgotPassword
    {
        public string userEMail { get; set; }
        public string otp { get; set; }
        public string firstName { get; set; }
        public string passWord { get; set; }
    }
}
