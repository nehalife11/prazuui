﻿namespace Contracts
{
    public class Asset
    {
        public int AssetId { get; set; }
        public string AssetName { get; set; }
    }
}
