﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
   public class ExecutionManager
    {
        public string ProjectName { get; set; } 
        public string Browser { get; set; }
        public string ReleaseName { get; set; }
        public string Url { get; set; }
        public string Snapshot { get; set; }
        public string HostName { get; set; }
        public string snapshotForAllPass{get; set;}
        public string LocalMachine { get; set; }
        public List<UploadFile> uploadFilesList = new List<UploadFile>();

    }

    public class UploadFile
    {
        public string fileName { get; set; }
        public string fileData { get; set; }
    }
}
