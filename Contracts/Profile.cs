﻿using System.Collections.Generic;

namespace Contracts
{
    public  class Profile
    {
        public Profile()
        {
            UserMap = new User();
            RoleMap = new Role();
            ProjectMap = new ProjectMaster();
        }
        public int ProfileId { get; set; }
        public User UserMap { get; set; }
        public Role RoleMap { get; set; }
        public ProjectMaster ProjectMap { get; set;}
        public List<Asset> AssetMap { get; set; }
    }
}
