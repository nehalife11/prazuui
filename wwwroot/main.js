(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./test-form/test-form.component */ "./src/app/test-form/test-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
    },
    {
        path: 'login',
        component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"]
    },
    {
        path: '',
        component: _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_4__["TestFormComponent"]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<app-navbar></app-navbar>\n\n<div class=\"container\">\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'PrazuUI';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./test-form/test-form.component */ "./src/app/test-form/test-form.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"],
                _test_form_test_form_component__WEBPACK_IMPORTED_MODULE_8__["TestFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-4 col-sm-offset-4 form-signin\">\n        <div class=\"row\">\n            <div class=\"col-sm-12 form-signin-icon text-center\">\n                <img src=\"../../assets/icons/user.svg\" alt=\"Kiwi standing on oval\">\n            </div>\n            <div class=\"col-sm-offset-1 col-sm-10 form-signin-inputs\">\n                <p class=\"title text-center\">Sign In</p>\n                \n                <input type=\"text\" placeholder=\"User Name\">\n\n                <input type=\"text\" placeholder=\"Password\">\n\n                <button class=\"btn btn-login\">Login</button>\n            </div>\n            <div class=\"col-sm-12 form-signin-register\">\n                <p>Don't have an account ? <span><a href=\"#\">REGISTER HERE</a></span></p>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-signin {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  position: relative;\n  top: 15vh; }\n  .form-signin .form-signin-icon {\n    position: relative;\n    top: -40px; }\n  .form-signin .form-signin-icon img {\n      border: 1px solid #ffffff;\n      border-radius: 40px; }\n  .form-signin .form-signin-inputs {\n    margin-top: -25px; }\n  .form-signin .form-signin-inputs input {\n      width: 100%;\n      margin-top: 20px;\n      padding: 12px;\n      border-radius: 8px;\n      border: 1px solid #ffffff; }\n  .form-signin .form-signin-inputs .title {\n      font-size: 2.2em;\n      color: #ffffff;\n      font-weight: 500; }\n  .form-signin .form-signin-inputs .btn-login {\n      width: 100%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-signin .form-signin-register {\n    text-align: center;\n    font-size: 1em;\n    color: #ffffff;\n    font-weight: 600; }\n  .form-signin .form-signin-register a {\n      color: #AACB2F;\n      font-weight: 700; }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginComponent = /** @class */ (function () {
    function LoginComponent() {
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n    <ul>\n        <li>\n            <a routerLink=\"\">\n                <i class=\"material-icons\">supervised_user_circle</i>\n            </a>\n        </li>\n        <li>\n            <a routerLink=\"posts\">\n                <i class=\"material-icons\">message</i>\n            </a>\n        </li>\n    </ul>\n</nav>"

/***/ }),

/***/ "./src/app/navbar/navbar.component.scss":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav {\n  background: #00357A; }\n  nav ul {\n    list-style-type: none;\n    padding: 0;\n    margin: 0; }\n  nav ul li a {\n      color: #fff;\n      padding: 20px; }\n  nav ul li .activated {\n      background-color: #00a8ff; }\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-4 col-sm-offset-4 form-register\">\n        <div class=\"row\">\n            <div class=\"col-sm-12 form-register-icon text-center\">\n                <img src=\"../../assets/icons/register.svg\" alt=\"Kiwi standing on oval\">\n            </div>\n            <div class=\"col-sm-offset-1 col-sm-10 form-register-inputs\">\n                <p class=\"title text-center\">Register</p>\n                \n                <input type=\"text\" placeholder=\"First Name\">\n\n                <input type=\"text\" placeholder=\"Last Name\">\n\n                <input type=\"text\" placeholder=\"Email\">\n\n                <input type=\"text\" placeholder=\"Password\">\n\n                <input type=\"text\" placeholder=\"Confirm Password\">\n\n                <button class=\"btn btn-login\">Register</button>\n            </div>\n            <div class=\"col-sm-12 form-register-login\">\n                <p>Already have an account ? <span><a href=\"#\">SIGN IN</a></span></p>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/register/register.component.scss":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-register {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  position: relative;\n  top: 15vh; }\n  .form-register .form-register-icon {\n    position: relative;\n    top: -40px; }\n  .form-register .form-register-icon img {\n      border: 1px solid #ffffff;\n      border-radius: 40px; }\n  .form-register .form-register-inputs {\n    margin-top: -25px; }\n  .form-register .form-register-inputs input {\n      width: 100%;\n      margin-top: 20px;\n      padding: 12px;\n      border-radius: 8px;\n      border: 1px solid #ffffff; }\n  .form-register .form-register-inputs .title {\n      font-size: 2.2em;\n      color: #ffffff;\n      font-weight: 500; }\n  .form-register .form-register-inputs .btn-login {\n      width: 100%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n  .form-register .form-register-login {\n    text-align: center;\n    font-size: 1em;\n    color: #ffffff;\n    font-weight: 600; }\n  .form-register .form-register-login a {\n      color: #AACB2F;\n      font-weight: 700; }\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegisterComponent = /** @class */ (function () {
    function RegisterComponent() {
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/run-tests.service.ts":
/*!**************************************!*\
  !*** ./src/app/run-tests.service.ts ***!
  \**************************************/
/*! exports provided: RunTestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunTestsService", function() { return RunTestsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RunTestsService = /** @class */ (function () {
    function RunTestsService(http) {
        this.http = http;
    }
    RunTestsService.prototype.postDetails = function (requestObject) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'text/xml' })
        };
        return this.http.post('http://localhost:8082/Automation/services/automation', requestObject, httpOptions).subscribe(function (result) {
            console.log("Result is:", result);
        }, function (error) { return console.log('There was an error: '); });
    };
    RunTestsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RunTestsService);
    return RunTestsService;
}());



/***/ }),

/***/ "./src/app/test-form/test-form.component.html":
/*!****************************************************!*\
  !*** ./src/app/test-form/test-form.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-offset-1 col-sm-10 form-test\">\n        <p class=\"title text-center\">USER DASHBOARD</p>\n        <div class=\"row\">\n            <div class=\"col-sm-12 form-test-fields\">\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                        <div>\n                            <label for=\"projName\">Project Name</label>\n                            <input id=\"projName\" [(ngModel)]=\"project\">\n                        </div>\n                        <div>\n                            <label for=\"projName\">Browser</label>\n                            <select>\n                                <option value=\"chrome\">Chrome</option>\n                                <option value=\"saab\">Safari</option>\n                                <option value=\"mercedes\">IE</option>\n                                <option value=\"audi\">Firefox</option>\n                            </select>\n                        </div>\n                        <div>\n                            <label for=\"projName\">Snapshot</label>\n                            <select>\n                                <option value=\"browser\">Browser</option>\n                            </select>\n                        </div>\n                        <div>\n                            <label for=\"projName\">Host Name</label>\n                            <input id=\"projName\">\n                        </div>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <div>\n                            <label for=\"release\">Release Name</label>\n                            <input id=\"release\" [(ngModel)]=\"release\">\n                        </div>\n                        <div>\n                            <label for=\"url\">Host Name</label>\n                            <input id=\"url\">\n                        </div>\n                        <div>\n                            <label for=\"snapshotType\">Snapshot Type</label>\n                            <select>\n                            <option value=\"chrome\">Yes</option>\n                            <option value=\"saab\">No</option>\n                            </select>\n                        </div>\n                        <div>\n                            <label for=\"projName\">Local Machine</label>\n                            <select>\n                            <option value=\"browser\">Local Machine</option>\n                            </select>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-offset-2 col-sm-8 form-test-uploads\">\n                <div class=\"row\">\n                    <div class=\"col-sm-12 upload-option\" style=\"display:flex\">\n                        <div class=\"col-sm-6 header\">Upload Execution Manager</div>\n                        <div class=\"col-sm-5 action\"><input id=\"fileUpload\" type=\"file\" class=\"btn\"></div>\n                    </div>\n                    <div class=\"col-sm-12 upload-option\" style=\"display:flex\">\n                        <div class=\"col-sm-6 header\">Upload Mapping</div>\n                        <div class=\"col-sm-5 action\"><input id=\"uploadMap\" type=\"file\" class=\"btn\"></div>\n                    </div>\n                    <div class=\"col-sm-12 upload-option\" style=\"display:flex\">\n                        <div class=\"col-sm-6 header\">Upload Test Creation</div>\n                        <div class=\"col-sm-5 action\"><input id=\"uploadTestCreation\" type=\"file\" class=\"btn\"></div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-12 form-test-results\">\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                        <button>Get Old Results</button>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <button (click)=\"onSubmit()\">Save & Continue</button>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/test-form/test-form.component.scss":
/*!****************************************************!*\
  !*** ./src/app/test-form/test-form.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-test {\n  background: rgba(255, 255, 255, 0.1);\n  border: 1px solid #cccccc;\n  border-radius: 10px;\n  position: relative;\n  top: 15vh;\n  padding: 20px; }\n  .form-test .title {\n    font-size: 2em;\n    font-weight: 700;\n    letter-spacing: 2px;\n    color: #ffffff; }\n  .form-test label {\n    width: 40%;\n    color: #ffffff;\n    font-weight: 500;\n    font-size: 1.2em; }\n  .form-test input, .form-test select {\n    width: 55%;\n    margin-top: 20px;\n    border-radius: 8px;\n    border: 2px solid #AACB2F;\n    height: 35px;\n    background: transparent;\n    padding: 0 10px;\n    color: #ffffff; }\n  .form-test .form-test-uploads {\n    margin-top: 40px;\n    color: #ffffff;\n    font-weight: 500;\n    font-size: 1.2em; }\n  .form-test .form-test-uploads .upload-option {\n      display: flex;\n      margin-top: 15px; }\n  .form-test .form-test-uploads input {\n      margin-top: 0;\n      width: 100%;\n      padding: 5px; }\n  .form-test .form-test-results {\n    margin-top: 40px;\n    text-align: center; }\n  .form-test .form-test-results button {\n      width: 80%;\n      margin: 30px 0;\n      font-size: 1.2em;\n      color: #ffffff;\n      font-weight: 500;\n      background: #00357A;\n      border-radius: 0;\n      padding: 15px 0;\n      letter-spacing: 2px; }\n"

/***/ }),

/***/ "./src/app/test-form/test-form.component.ts":
/*!**************************************************!*\
  !*** ./src/app/test-form/test-form.component.ts ***!
  \**************************************************/
/*! exports provided: TestFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestFormComponent", function() { return TestFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _run_tests_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../run-tests.service */ "./src/app/run-tests.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TestFormComponent = /** @class */ (function () {
    function TestFormComponent(data) {
        this.data = data;
        this.project = '';
        this.release = '';
        this.reqObj = {};
    }
    TestFormComponent.prototype.ngOnInit = function () {
        document.getElementById('fileUpload').addEventListener('change', this.onFileUpload.bind(this));
    };
    TestFormComponent.prototype.onFileUpload = function (evt) {
    };
    TestFormComponent.prototype.onSubmit = function () {
    };
    ;
    TestFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-test-form',
            template: __webpack_require__(/*! ./test-form.component.html */ "./src/app/test-form/test-form.component.html"),
            styles: [__webpack_require__(/*! ./test-form.component.scss */ "./src/app/test-form/test-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_run_tests_service__WEBPACK_IMPORTED_MODULE_1__["RunTestsService"]])
    ], TestFormComponent);
    return TestFormComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/pkalavendi/Projects/Prazu/PrazuUI/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map